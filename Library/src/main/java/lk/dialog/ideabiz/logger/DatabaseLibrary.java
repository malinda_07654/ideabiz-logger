package lk.dialog.ideabiz.logger;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * Created by Malinda on 7/10/2015.
 */
public class DatabaseLibrary {

    private static DataSource DatabaseSource = null;
    private static Logger logger;

    public DataSource getDatabaseSource() {
        return DatabaseSource;
    }

    public void setDatabaseSource(DataSource databaseSource) {
        DatabaseSource = databaseSource;
    }

    public DatabaseLibrary() {
        logger = Logger.getLogger(DatabaseLibrary.class);
        if (DatabaseSource == null)
            DatabaseFromConfig();
    }

    public DatabaseLibrary(DataSource dataSource) {
        logger = Logger.getLogger(DatabaseLibrary.class);
        setDatabaseSource(dataSource);
    }

    public void DatabaseFromConfig() {
        logger.info("Initializing Database from config");
        Properties props = new Properties();
        FileInputStream fis = null;
        try {
            Boolean loaded = false;
            try {
                props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/logger.properties"));
                loaded = true;
            } catch (Exception e) {
                logger.error("Config Load filed from web App. trying to load from LIB: ");
            }
            if (!loaded)
                props.load(DirectLogger.class.getResourceAsStream("/logger.properties"));

            ComboPooledDataSource mysqlDataSource = new ComboPooledDataSource();
            mysqlDataSource.setJdbcUrl(props.getProperty("jdbc.LOGGER.url"));
            mysqlDataSource.setUser(props.getProperty("jdbc.LOGGER.username"));
            mysqlDataSource.setPassword(props.getProperty("jdbc.LOGGER.password"));
            mysqlDataSource.setMaxPoolSize(Integer.parseInt(props.getProperty("jdbc.LOGGER.maxPool")));
            mysqlDataSource.setMinPoolSize(Integer.parseInt(props.getProperty("jdbc.LOGGER.minPool")));
            mysqlDataSource.setMaxStatements(Integer.parseInt(props.getProperty("jdbc.LOGGER.maxStatements")));
            mysqlDataSource.setTestConnectionOnCheckout(Boolean.parseBoolean(props.getProperty("jdbc.LOGGER.testConnection")));
            DatabaseSource = mysqlDataSource;
        } catch (Exception e) {
            logger.error("DB Init Error : " + e.getMessage(), e);
        }

    }


}
