package lk.dialog.ideabiz.logger.model.impl;

import com.google.common.base.CharMatcher;
import lk.dialog.ideabiz.library.NumberFormat;
import lk.dialog.ideabiz.logger.model.LoggerRequest;
import lk.dialog.ideabiz.logger.model.enumarator.SMSType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Malinda on 9/9/2015.
 */
public class BalanceCheck implements LoggerRequest {
    String version = null;
    Integer appid = null;

    String operator =null;
    String address = null;
    String status = null;
    Long CG_Time;
    Long total_time;



    private static String sqlQuery = "INSERT INTO `payment_balancecheck` (`id`,  `datetime`, `unix_time` , `version`,`operator`, `appid`, `address`, `status`,`CG_time`,`total_time`) " +
            "VALUES (NULL, CURRENT_TIMESTAMP(), UNIX_TIMESTAMP (NOW()), ?, ?, ?, ?, ?, ?, ?);";

    @Override
    public PreparedStatement prepareSQL(Connection sqlConnection) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.prepareStatement(sqlQuery);
        formatAddress();
        preparedStatement.setString(1, this.version);
        preparedStatement.setString(2, this.operator);
        preparedStatement.setInt(3, this.appid);
        preparedStatement.setString(4, this.address);
        preparedStatement.setString(5, this.status);
        if (this.CG_Time == null)
            CG_Time = new Long(0);
        preparedStatement.setLong(6, this.CG_Time);
        if (this.total_time == null)
            total_time = new Long(0);
        preparedStatement.setLong(7, this.total_time);

        return preparedStatement;
    }

    public BalanceCheck(String version, String operator, Integer appid, String address, String status, Long CGTime, Long totalTime) {
        this.version = version;
        this.operator = operator;
        this.appid = appid;
        this.address = address;
        this.status = status;
        this.CG_Time = CGTime;
        this.total_time = totalTime;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getAppid() {
        return appid;
    }

    public void setAppid(Integer appid) {
        this.appid = appid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCG_Time() {
        return CG_Time;
    }

    public void setCG_Time(Long CG_Time) {
        this.CG_Time = CG_Time;
    }

    public Long getTotal_time() {
        return total_time;
    }

    public void setTotal_time(Long total_time) {
        this.total_time = total_time;
    }

    public void formatAddress() {
        if (this.address != null)
            this.address = NumberFormat.formatNumber(this.address);
    }

}
