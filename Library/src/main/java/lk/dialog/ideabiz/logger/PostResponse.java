package lk.dialog.ideabiz.logger;

import java.util.List;
import java.util.Map;

/**
 * Created by Malinda on 6/29/2015.
 */
public class PostResponse {
    String response;
    Map<String,List<String>> headers;

    public PostResponse(){
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Map<String,List<String>> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String,List<String>> headers) {
        this.headers = headers;
    }
}
