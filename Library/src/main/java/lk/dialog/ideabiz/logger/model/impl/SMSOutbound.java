package lk.dialog.ideabiz.logger.model.impl;

import com.google.common.base.CharMatcher;
import lk.dialog.ideabiz.library.NumberFormat;
import lk.dialog.ideabiz.logger.model.LoggerRequest;
import lk.dialog.ideabiz.logger.model.enumarator.SMSType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malinda on 9/9/2015.
 */
public class SMSOutbound implements LoggerRequest {
    static Integer SMS_UNICODE_LENGTH = 70;
    static Integer SMS_STANDARD_LENGTH = 160;

    String id;
    String datetime;
    Long unix_time;

    String version = null;
    Integer appid = null;
    String operator = null;

    String address = null;
    String addressType = null;
    Integer addressCount = 0;
    String senderAddress = null;
    String senderName = null;
    String finalSender = null;
    String requestClientCorrelator = null;
    String responseClientCorrelator = null;
    SMSType type = null;
    Integer messageCount = 0;
    Integer length = 0;
    String messageStatus = null;

    //200,404,
    String appStatusCode = null;
    String messageRef;
    String batchRef;
    String deliveredTime;

    private static String sqlQuery = "INSERT INTO `smsmessaging_outbound` (`id`,  `datetime`, `unix_time` , `version`, `operator`, `appid`, `address`,`addressType`, `addressCount`, `senderAddress`, `senderName`, `finalSender`, `requestClientCorrelator`, `responseClientCorrelator`, `type`, `messageCount`, `length`, `messageStatus`, `appStatusCode`,`messageRef`,`batchRef`,`deleveredTime`) " +
            "VALUES (NULL, CURRENT_TIMESTAMP(), UNIX_TIMESTAMP (NOW()), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NULL);";

    @Override
    public PreparedStatement prepareSQL(Connection sqlConnection) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, this.version);
        preparedStatement.setString(2, this.operator);

        preparedStatement.setInt(3, this.appid);

        preparedStatement.setString(4, this.address);
        preparedStatement.setString(5, this.addressType);
        preparedStatement.setInt(6, this.addressCount);
        preparedStatement.setString(7, this.senderAddress);
        preparedStatement.setString(8, this.senderName);
        preparedStatement.setString(9, this.finalSender);
        preparedStatement.setString(10, this.requestClientCorrelator);
        preparedStatement.setString(11, this.responseClientCorrelator);
        if (type == null)
            type = SMSType.STANDARD;
        preparedStatement.setString(12, this.type.toString());
        preparedStatement.setInt(13, this.messageCount);
        preparedStatement.setInt(14, this.length);
        preparedStatement.setString(15, this.messageStatus);
        preparedStatement.setString(16, this.appStatusCode);
        preparedStatement.setString(17, this.messageRef);
        preparedStatement.setString(18, this.batchRef);

        return preparedStatement;

    }



    public SMSOutbound(String version, String operator, Integer appid, String address,String addressType, String senderAddress, String senderName, String finalSender, String requestClientCorrelator, String responseClientCorrelator, String messageStatus, String message, String messageRef, String batchRef) {
        this.version = version;
        this.operator = operator;
        this.appid = appid;
        this.senderAddress = senderAddress;
        this.senderName = senderName;
        this.requestClientCorrelator = requestClientCorrelator;
        this.responseClientCorrelator = responseClientCorrelator;
        this.messageStatus = messageStatus;
        this.finalSender = finalSender;
        setType(message);
        setSize(message);
        this.address = address;
        this.addressType = addressType;
        this.messageRef = messageRef;
        this.batchRef = batchRef;
    }


    public void setAddress(ArrayList<String> address) {
        this.address = "";
        this.addressCount = 0;
        if (address == null)
            return;

        if (address.size() == 1) {
            this.address = address.get(0);
            addressCount = 1;
        }
        for (String cur : address) {
            cur = NumberFormat.formatNumber(cur);
            this.address = this.address.concat(cur);
            this.address = this.address.concat(";");
            addressCount++;
        }
    }

    public void setType(String message) {
        if (CharMatcher.ASCII.matchesAllOf(message)) {
            type = SMSType.STANDARD;
        } else {
            type = SMSType.UNICODE;
        }
    }

    public void setSize(String message) {
        length = message.length();
        if (type == null || type == SMSType.STANDARD) {
            calcMessageCount(length, SMS_STANDARD_LENGTH, 4);
        } else if (type == SMSType.UNICODE) {
            calcMessageCount(length, SMS_UNICODE_LENGTH, 8);
        }
    }

    public void calcMessageCount(Integer length, Integer maxSize, Integer multipleGap) {
        if (length <= maxSize) {
            this.messageCount = 1;
        } else {
            int mod = (length % (maxSize - multipleGap));
            this.messageCount = (length - mod) / (maxSize - multipleGap);
            if (mod > 0)
                this.messageCount++;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public Long getUnix_time() {
        return unix_time;
    }

    public void setUnix_time(Long unix_time) {
        this.unix_time = unix_time;
    }

    public Integer getAddressCount() {
        return addressCount;
    }

    public void setAddressCount(Integer addressCount) {
        this.addressCount = addressCount;
    }

    public String getFinalSender() {
        return finalSender;
    }

    public void setFinalSender(String finalSender) {
        this.finalSender = finalSender;
    }

    public String getMessageRef() {
        return messageRef;
    }

    public void setMessageRef(String messageRef) {
        this.messageRef = messageRef;
    }

    public String getBatchRef() {
        return batchRef;
    }

    public void setBatchRef(String batchRef) {
        this.batchRef = batchRef;
    }

    public String getDeliveredTime() {
        return deliveredTime;
    }

    public void setDeliveredTime(String deliveredTime) {
        this.deliveredTime = deliveredTime;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getAppid() {
        return appid;
    }

    public void setAppid(Integer appid) {
        this.appid = appid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getRequestClientCorrelator() {
        return requestClientCorrelator;
    }

    public void setRequestClientCorrelator(String requestClientCorrelator) {
        this.requestClientCorrelator = requestClientCorrelator;
    }

    public String getResponseClientCorrelator() {
        return responseClientCorrelator;
    }

    public void setResponseClientCorrelator(String responseClientCorrelator) {
        this.responseClientCorrelator = responseClientCorrelator;
    }

    public SMSType getType() {
        return type;
    }

    public void setType(SMSType type) {
        this.type = type;
    }

    public Integer getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(Integer messageCount) {
        this.messageCount = messageCount;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(String messageStatus) {
        this.messageStatus = messageStatus;
    }

    public String getAppStatusCode() {
        return appStatusCode;
    }

    public void setAppStatusCode(String appStatusCode) {
        this.appStatusCode = appStatusCode;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}
