package lk.dialog.ideabiz.logger.model.impl;

import com.google.common.base.CharMatcher;
import lk.dialog.ideabiz.library.NumberFormat;
import lk.dialog.ideabiz.logger.model.LoggerRequest;
import lk.dialog.ideabiz.logger.model.enumarator.SMSType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Malinda on 9/9/2015.
 */
public class SMSInbound implements LoggerRequest {
    static Integer SMS_UNICODE_LENGTH = 70;
    static Integer SMS_STANDARD_LENGTH = 160;

    String version = null;
    Integer appid = null;
    String operator = null;

    String destinationAddress = null;
    String messageId = null;
    String senderAddress = null;
    String addressType;
    Integer length = null;
    SMSType type = null;
    Integer messageCount = 0;
    String status = null;
    //200,404,
    String appStatusCode = null;

    private static String sqlQuery = "INSERT INTO `smsmessaging_inbound` (`id`,  `datetime`, `unix_time` , `version`, `operator`, `appid`, `destinationAddress`, `messageId`, `senderAddress`,`addressType`, `length`, `type`, `messageCount`, `status`, `appStatusCode`) " +
            "VALUES (NULL, CURRENT_TIMESTAMP(), UNIX_TIMESTAMP (NOW()), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

    @Override
    public PreparedStatement prepareSQL(Connection sqlConnection) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.prepareStatement(sqlQuery);

        formatAddress();

        preparedStatement.setString(1, this.version);
        preparedStatement.setString(2, this.version);
        preparedStatement.setInt(3, this.appid);

        preparedStatement.setString(4, this.destinationAddress);
        preparedStatement.setString(5, this.messageId);
        preparedStatement.setString(6, this.senderAddress);
        preparedStatement.setString(7, this.addressType);
        preparedStatement.setInt(8, this.length);
        if (type == null)
            type = SMSType.STANDARD;
        preparedStatement.setString(9, this.type.toString());
        preparedStatement.setInt(10, this.messageCount);
        preparedStatement.setString(11, this.status);
        preparedStatement.setString(12, this.appStatusCode);

        return preparedStatement;

    }


    public SMSInbound(String version, String operator, Integer appid, String destinationAddress, String messageId, String senderAddress, String addressType, Integer length, SMSType type, int messageCount, String appStatusCode, String status) {
        this.version = version;
        this.operator = operator;
        this.appid = appid;
        this.destinationAddress = destinationAddress;
        this.messageId = messageId;
        this.senderAddress = senderAddress;
        this.length = length;
        this.type = type;
        this.messageCount = messageCount;
        this.appStatusCode = appStatusCode;
        this.status = status;
        this.addressType = addressType;
    }

    public SMSInbound(String version, String operator, Integer appid, String destinationAddress, String messageId, String senderAddress,String addressType, String appStatusCode, String message, String status) {
        this.version = version;
        this.operator = operator;
        this.appid = appid;
        this.destinationAddress = destinationAddress;
        this.messageId = messageId;
        this.senderAddress = senderAddress;
        this.appStatusCode = appStatusCode;

        setType(message);
        setSize(message);
        this.addressType = addressType;

    }

    public void setType(String message) {
        if (CharMatcher.ASCII.matchesAllOf(message)) {
            type = SMSType.STANDARD;
        } else {
            type = SMSType.UNICODE;
        }
    }

    public void setSize(String message) {
        length = message.length();
        if (type == null || type == SMSType.STANDARD) {
            calcMessageCount(length, SMS_STANDARD_LENGTH, 4);
        } else if (type == SMSType.UNICODE) {
            calcMessageCount(length, SMS_UNICODE_LENGTH, 8);
        }
    }

    public void calcMessageCount(Integer length, Integer maxSize, Integer multipleGap) {
        if (length <= maxSize) {
            this.messageCount = 1;
        } else {
            int mod = (length % (maxSize - multipleGap));
            this.messageCount = (length - mod) / (maxSize - multipleGap);
            if (mod > 0)
                this.messageCount++;
        }
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getAppid() {
        return appid;
    }

    public void setAppid(Integer appid) {
        this.appid = appid;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public SMSType getType() {
        return type;
    }

    public void setType(SMSType type) {
        this.type = type;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    public String getAppStatusCode() {
        return appStatusCode;
    }

    public void setAppStatusCode(String appStatusCode) {
        this.appStatusCode = appStatusCode;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public void formatAddress() {
        if (this.senderAddress != null)
            this.senderAddress = NumberFormat.formatNumber(this.senderAddress);

        if (this.destinationAddress != null) {
            this.destinationAddress = this.destinationAddress.replace("tel:+", "");
            this.destinationAddress = this.destinationAddress.replace("tel:", "");
        }
    }
}
