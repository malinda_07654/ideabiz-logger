package lk.dialog.ideabiz.logger.model;

/**
 * Created by Malinda on 9/15/2015.
 */
public class MessageAddress {
    String address;
    String deliveryStatus;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }
}
