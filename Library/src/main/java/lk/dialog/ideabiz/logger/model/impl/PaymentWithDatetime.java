package lk.dialog.ideabiz.logger.model.impl;

import lk.dialog.ideabiz.library.NumberFormat;
import lk.dialog.ideabiz.logger.model.LoggerRequest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Malinda on 9/9/2015.
 */
public class PaymentWithDatetime implements LoggerRequest {
    String datetime;
    String version = null;
    Integer appid = null;
    String operator = null;

    String msisdn = null;
    String chargingtype = null;
    String reasoncode = null;
    String dialog_ref = null;
    String application_ref = null;
    String cg_ref = null;
    Double amount = null;
    Double tax = null;
    String currency = null;
    String description = null;
    String channel = null;
    String client_correlator = null;
    String requested_status = null;
    String result_status = null;
    String result_description = null;
    Long CG_Time;
    Long total_time;

    private static String sqlQuery = "INSERT INTO `payment_transaction` (`id`,  `datetime`, `unix_time` , `version`, `operator`, `appid`,`msisdn`, `chargingtype`, `reasoncode`, `dialog_ref`, `application_ref`, `cg_ref`, `amount`, `tax`, `currency`, `description`, `channel`, `client_correlator`,`client_correlator_hash`, `requested_status`, `result_status`, `result_description`,`CG_time`,`total_time`) " +
            "VALUES (NULL, ?, UNIX_TIMESTAMP(?), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, MD5(?), ?, ?, ? , ?, ?);";

    @Override
    public PreparedStatement prepareSQL(Connection sqlConnection) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.prepareStatement(sqlQuery);

        formatAddress();

        preparedStatement.setString(1, this.datetime);
        preparedStatement.setString(2, this.datetime);
        preparedStatement.setString(3, this.version);
        preparedStatement.setString(4, this.operator);
        preparedStatement.setInt(5, this.appid);
        preparedStatement.setString(6, this.msisdn);
        preparedStatement.setString(7, this.chargingtype);
        preparedStatement.setString(8, this.reasoncode);
        preparedStatement.setString(9, this.dialog_ref);
        preparedStatement.setString(10, this.application_ref);
        preparedStatement.setString(11, this.cg_ref);
        preparedStatement.setDouble(12, this.amount);
        preparedStatement.setDouble(13, this.tax);
        preparedStatement.setString(14, this.currency);
        preparedStatement.setString(15, this.description);
        preparedStatement.setString(16, this.channel);
        preparedStatement.setString(17, this.client_correlator);
        preparedStatement.setString(18, this.client_correlator);
        preparedStatement.setString(19, this.requested_status);
        preparedStatement.setString(20, this.result_status);
        preparedStatement.setString(21, this.result_description);

        if (this.CG_Time == null)
            CG_Time = new Long(0);
        preparedStatement.setLong(22, this.CG_Time);
        if (this.total_time == null)
            total_time = new Long(0);
        preparedStatement.setLong(23, this.total_time);

        return preparedStatement;

    }

    public PaymentWithDatetime(String version, String operator, Integer appid, String datetime, String msisdn, String chargingtype, String reasoncode, String dialog_ref, String application_ref, String cg_ref, Double amount, Double tax, String currency, String description, String channel, String client_correlator, String requested_status, String result_status, String result_description, Long CG_Time, Long total_time) {
        this.version = version;
        this.operator = operator;
        this.appid = appid;
        this.datetime = datetime;
        this.msisdn = msisdn;
        this.chargingtype = chargingtype;
        this.reasoncode = reasoncode;
        this.dialog_ref = dialog_ref;
        this.application_ref = application_ref;
        this.cg_ref = cg_ref;
        this.amount = amount;
        this.tax = tax;
        this.currency = currency;
        this.description = description;
        this.channel = channel;
        this.client_correlator = client_correlator;
        this.requested_status = requested_status;
        this.result_status = result_status;
        this.result_description = result_description;
        this.CG_Time = CG_Time;
        this.total_time = total_time;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getAppid() {
        return appid;
    }

    public void setAppid(Integer appid) {
        this.appid = appid;
    }

    public String getChargingtype() {
        return chargingtype;
    }

    public void setChargingtype(String chargingtype) {
        this.chargingtype = chargingtype;
    }

    public String getReasoncode() {
        return reasoncode;
    }

    public void setReasoncode(String reasoncode) {
        this.reasoncode = reasoncode;
    }

    public String getDialog_ref() {
        return dialog_ref;
    }

    public void setDialog_ref(String dialog_ref) {
        this.dialog_ref = dialog_ref;
    }

    public String getApplication_ref() {
        return application_ref;
    }

    public void setApplication_ref(String application_ref) {
        this.application_ref = application_ref;
    }

    public String getCg_ref() {
        return cg_ref;
    }

    public void setCg_ref(String cg_ref) {
        this.cg_ref = cg_ref;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public static String getSqlQuery() {
        return sqlQuery;
    }

    public static void setSqlQuery(String sqlQuery) {
        PaymentWithDatetime.sqlQuery = sqlQuery;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getClient_correlator() {
        return client_correlator;
    }

    public void setClient_correlator(String client_correlator) {
        this.client_correlator = client_correlator;
    }

    public String getRequested_status() {
        return requested_status;
    }

    public void setRequested_status(String requested_status) {
        this.requested_status = requested_status;
    }

    public String getResult_status() {
        return result_status;
    }

    public void setResult_status(String result_status) {
        this.result_status = result_status;
    }

    public String getResult_description() {
        return result_description;
    }

    public void setResult_description(String result_description) {
        this.result_description = result_description;
    }

    public Long getTotal_time() {
        return total_time;
    }

    public void setTotal_time(Long total_time) {
        this.total_time = total_time;
    }

    public Long getCG_Time() {
        return CG_Time;
    }

    public void setCG_Time(Long CG_Time) {
        this.CG_Time = CG_Time;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void formatAddress() {
        if (this.msisdn != null)
            this.msisdn = NumberFormat.formatNumber(this.msisdn);
    }
}
