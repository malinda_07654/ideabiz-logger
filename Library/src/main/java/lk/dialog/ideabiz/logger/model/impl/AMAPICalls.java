package lk.dialog.ideabiz.logger.model.impl;

import lk.dialog.ideabiz.library.NumberFormat;
import lk.dialog.ideabiz.logger.model.LoggerRequest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Malinda on 9/9/2015.
 */
public class AMAPICalls implements LoggerRequest {
    String datetime;
    String version = null;
    Integer appid = null;
    String API =null;
    Long count = null;
    Long AM = null;


    private static String sqlQuery = "INSERT INTO `am_apicalls` (`id`,  `datetime`, `unix_time` , `version`,`appid`, `API`, `count`,`AM`) " +
            "VALUES (NULL, ? , UNIX_TIMESTAMP (?), ?, ?, ?, ?,?);";

    @Override
    public PreparedStatement prepareSQL(Connection sqlConnection) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.prepareStatement(sqlQuery);

        preparedStatement.setString(1, this.datetime);
        preparedStatement.setString(2, this.datetime);
        preparedStatement.setString(3, this.version);
        preparedStatement.setLong(4, this.appid);
        preparedStatement.setString(5, this.API);
        preparedStatement.setLong(6, this.count);
        preparedStatement.setLong(7, this.AM);

        return preparedStatement;

    }

    public AMAPICalls(String datetime, String version, Integer appid, String API, Long count, Long AM) {
        this.datetime = datetime;
        this.version = version;
        this.appid = appid;
        this.API = API;
        this.count = count;
        this.AM = AM;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getAppid() {
        return appid;
    }

    public void setAppid(Integer appid) {
        this.appid = appid;
    }

    public String getAPI() {
        return API;
    }

    public void setAPI(String API) {
        this.API = API;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
