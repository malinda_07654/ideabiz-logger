package lk.dialog.ideabiz.logger.model.impl;

import lk.dialog.ideabiz.library.NumberFormat;
import lk.dialog.ideabiz.logger.model.LoggerRequest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Malinda on 9/9/2015.
 */
public class RequestURL implements LoggerRequest {
    String version = null;
    Long appid = null;

    String URL =null;
    String API = null;
    String APIVersion = null;
    String APIMethod = null;
    String status;
    String errorCode;
    Integer statusCode;
    Long responseTime;



    private static String sqlQuery = "INSERT INTO `request_url` (`id`, `datetime`, `unix_time`, `version`, `appid`, `URL`, `URLHash`, `API`, `APIVersion`, `APIMethod`, `status`, `errorCode`, `statusCode`, `responseTime`) " +
            " VALUES (NULL, CURRENT_TIMESTAMP , UNIX_TIMESTAMP(NOW()), ?, ? , ?, MD5(?), ?, ?, ?, ?, ?, ? , ?);";

    @Override
    public PreparedStatement prepareSQL(Connection sqlConnection) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.prepareStatement(sqlQuery);

        preparedStatement.setString(1, this.version);

        preparedStatement.setLong(2, this.appid);
        preparedStatement.setString(3, this.URL);
        preparedStatement.setString(4, this.URL);
        preparedStatement.setString(5, this.API);
        preparedStatement.setString(6, this.APIVersion);
        preparedStatement.setString(7, this.APIMethod);
        preparedStatement.setString(8, this.status);
        preparedStatement.setString(9, this.errorCode);
        preparedStatement.setInt(10, this.statusCode);
        preparedStatement.setLong(11, this.responseTime);
        return preparedStatement;
    }

    public RequestURL(String version, Long appid, String URL, String API, String APIVersion, String APIMethod, String status, String errorCode, Integer statusCode, Long responseTime) {
        this.version = version;
        this.appid = appid;
        this.URL = URL;
        this.API = API;
        this.APIVersion = APIVersion;
        this.APIMethod = APIMethod;
        this.status = status;
        this.errorCode = errorCode;
        this.statusCode = statusCode;
        this.responseTime = responseTime;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Long getAppid() {
        return appid;
    }

    public void setAppid(Long appid) {
        this.appid = appid;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getAPI() {
        return API;
    }

    public void setAPI(String API) {
        this.API = API;
    }

    public String getAPIVersion() {
        return APIVersion;
    }

    public void setAPIVersion(String APIVersion) {
        this.APIVersion = APIVersion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Long responseTime) {
        this.responseTime = responseTime;
    }

    public String getAPIMethod() {
        return APIMethod;
    }

    public void setAPIMethod(String APIMethod) {
        this.APIMethod = APIMethod;
    }
}
