package lk.dialog.ideabiz.logger.model.impl;

import lk.dialog.ideabiz.library.NumberFormat;
import lk.dialog.ideabiz.logger.model.LoggerRequest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Malinda on 9/9/2015.
 */
public class Location implements LoggerRequest {
    String version = null;
    Integer appid = null;
    String operator =null;

    String address = null;
    Integer requestedAccuracy = null;
    String locationRetrievalStatus = null;
    Double accuracy = null;
    String status=null;
    Long LBS_time;
    Long total_time;


    private static String sqlQuery = "INSERT INTO `location_lbs` (`id`,  `datetime`, `unix_time` , `version`,`operator`, `appid`, `address`, `requestedAccuracy`, `locationRetrievalStatus`, `accuracy`, `status`,`LBS_time`,`total_time`) " +
            "VALUES (NULL, CURRENT_TIMESTAMP(), UNIX_TIMESTAMP (NOW()), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

    @Override
    public PreparedStatement prepareSQL(Connection sqlConnection) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.prepareStatement(sqlQuery);
        formatAddress();
        preparedStatement.setString(1, this.version);
        preparedStatement.setString(2, this.operator);
        preparedStatement.setInt(3, this.appid);
        preparedStatement.setString(4, this.address);

        if(this.requestedAccuracy==null)
            this.requestedAccuracy=0;
        preparedStatement.setInt(5, this.requestedAccuracy);
        preparedStatement.setString(6, this.locationRetrievalStatus);
        preparedStatement.setDouble(7, this.accuracy);
        preparedStatement.setString(8, this.status);


        if (this.LBS_time == null)
            LBS_time = new Long(0);
        preparedStatement.setLong(9, this.LBS_time);
        if (this.total_time == null)
            total_time = new Long(0);
        preparedStatement.setLong(10, this.total_time);
        return preparedStatement;

    }

    public Location(String version,String operator, Integer appid, String address, Integer requestedAccuracy, String locationRetrievalStatus, Double accuracy, String status,Long LBS_time,Long total_time) {
        this.version = version;
        this.operator = operator;
        this.appid = appid;
        this.address = address;
        this.requestedAccuracy = requestedAccuracy;
        this.locationRetrievalStatus = locationRetrievalStatus;
        this.accuracy = accuracy;
        this.status = status;
        this.LBS_time = LBS_time;
        this.total_time = total_time;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getAppid() {
        return appid;
    }

    public void setAppid(Integer appid) {
        this.appid = appid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getRequestedAccuracy() {
        return requestedAccuracy;
    }

    public void setRequestedAccuracy(Integer requestedAccuracy) {
        this.requestedAccuracy = requestedAccuracy;
    }

    public String getLocationRetrievalStatus() {
        return locationRetrievalStatus;
    }

    public void setLocationRetrievalStatus(String locationRetrievalStatus) {
        this.locationRetrievalStatus = locationRetrievalStatus;
    }

    public Double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getLBS_time() {
        return LBS_time;
    }

    public void setLBS_time(Long LBS_time) {
        this.LBS_time = LBS_time;
    }

    public Long getTotal_time() {
        return total_time;
    }

    public void setTotal_time(Long total_time) {
        this.total_time = total_time;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public void formatAddress(){
        if(this.address!=null)
            this.address = NumberFormat.formatNumber(this.address);
    }
}
