package lk.dialog.ideabiz.logger;


import com.google.gson.Gson;
import lk.dialog.ideabiz.logger.model.LoggerRequest;
import org.apache.log4j.*;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.sql.Connection;
import java.util.Properties;

/**
 * Created by Malinda on 9/10/2015.
 */
public class DirectLogger {
    private static Logger logger;
    static DatabaseLibrary databaseLibrary = null;
    public static Gson gson = new Gson();
    public static Boolean bypasslog = false;
    public static Boolean bypassdb = false;

    public static String logPath = null;
    public static String logPrefix = null;
    public static String logSuffix = null;


    public DirectLogger() {
        logger = Logger.getLogger(DirectLogger.class);

        loadConfig();
        if (bypassdb) {
            logger.info("Bypass DB construct");
            return;
        } else {
            databaseLibrary = new DatabaseLibrary();
        }

    }

    public DirectLogger(DataSource dataSource) {
        logger = Logger.getLogger(DirectLogger.class);
        loadConfig();

        if (bypassdb) {
            logger.info("Bypass DB construct");
            return;
        } else {
            databaseLibrary = new DatabaseLibrary(dataSource);
        }


    }

    public static void addLog(LoggerRequest log) {

        if (bypassdb) {
            if (!bypasslog) {
                logger = Logger.getLogger(log.getClass());
                logger.info("RECORD:SKIP:" + gson.toJson(log));
            }
            return;
        }

        try {
            Connection dbConnection = databaseLibrary.getDatabaseSource().getConnection();
            DBRunner runner = new DBRunner(dbConnection, log, bypasslog);
            runner.start();
        } catch (Exception e) {
           logger.error(e.getMessage(),e);
        }
    }

    public static void loadConfig() {
        logger.info("Initializing  config");
        Properties props = new Properties();
        Properties log4j = new Properties();
        FileInputStream fis = null;
        try {
            boolean loaded = false;
            try {
                props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/logger.properties"));
                loaded = true;
            } catch (Exception e) {
                logger.error("Config Load filed from App. trying to load from LIB: ");
            }
            if (!loaded)
                props.load(DirectLogger.class.getResourceAsStream("/logger.properties"));

            bypasslog = Boolean.parseBoolean(props.getProperty("app.bypasslog"));
            logger.info("bypasslog : " + bypasslog.toString());

            bypassdb = Boolean.parseBoolean(props.getProperty("app.bypassdb"));
            logger.info("bypassdb : " + bypassdb.toString());

            Boolean initLog4j = false;
            initLog4j = Boolean.parseBoolean(props.getProperty("app.initLog4j"));
            logger.info("initLog4j : " + initLog4j.toString());
            loaded = false;

            if (initLog4j) {
                try {
                    log4j.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/log4j.properties"));
                    loaded = true;
                } catch (Exception e) {
                    logger.error("Config Load log4j filed : " + e.getMessage());
                }
                if (!loaded)
                    log4j.load(DirectLogger.class.getResourceAsStream("/log4j.properties"));
                PropertyConfigurator.configure(log4j);
            }


        } catch (Exception e) {
            logger.error("Config Error : " + e.getMessage(), e);
        }
    }
}
