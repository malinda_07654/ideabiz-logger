package lk.dialog.ideabiz.logger.model.impl;

import lk.dialog.ideabiz.library.NumberFormat;
import lk.dialog.ideabiz.logger.model.LoggerRequest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Malinda on 9/9/2015.
 */
public class DigitalRewards implements LoggerRequest {
    String version = null;
    Integer appid = null;
    String operator =null;

    String type = null;
    String action = null;
    Double amount = null;
    String msisdn = null;
    String clientCorrelator = null;
    String rewardsTag = null;
    String pin = null;
    String message = null;
    String status = null;

    private static String sqlQuery = "INSERT INTO `digitalrewards_reward` (`id`,  `datetime`, `unix_time` , `version`, `appid`, `type`, `action`, `amount`,`msisdn`, `clientCorrelator`, `rewardsTag`, `pin`, `message`, `status`) " +
            " VALUES (NULL, CURRENT_TIMESTAMP(), UNIX_TIMESTAMP (NOW()), ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ? );";

    @Override
    public PreparedStatement prepareSQL(Connection sqlConnection) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.prepareStatement(sqlQuery);

        formatAddress();
        preparedStatement.setString(1, this.version);
        preparedStatement.setInt(2, this.appid);
        preparedStatement.setString(3, this.type);
        preparedStatement.setString(4, this.action);
        preparedStatement.setDouble(5, this.amount);
        preparedStatement.setString(6, this.msisdn);
        preparedStatement.setString(7, this.clientCorrelator);
        preparedStatement.setString(8, this.rewardsTag);
        preparedStatement.setString(9, this.pin);
        preparedStatement.setString(10, this.message);
        preparedStatement.setString(11, this.status);

        return preparedStatement;
    }

    public DigitalRewards(String version, Integer appid, String type, String action, Double amount, String msisdn, String clientCorrelator, String rewardsTag, String pin, String message, String status) {
        this.version = version;
        this.appid = appid;
        this.type = type;
        this.action = action;
        this.amount = amount;
        this.msisdn = msisdn;
        this.clientCorrelator = clientCorrelator;
        this.rewardsTag = rewardsTag;
        this.pin = pin;
        this.message = message;
        this.status = status;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getAppid() {
        return appid;
    }

    public void setAppid(Integer appid) {
        this.appid = appid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getClientCorrelator() {
        return clientCorrelator;
    }

    public void setClientCorrelator(String clientCorrelator) {
        this.clientCorrelator = clientCorrelator;
    }

    public String getRewardsTag() {
        return rewardsTag;
    }

    public void setRewardsTag(String rewardsTag) {
        this.rewardsTag = rewardsTag;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public void formatAddress() {
        if (this.msisdn != null)
            this.msisdn = NumberFormat.formatNumber(this.msisdn);
    }
}
