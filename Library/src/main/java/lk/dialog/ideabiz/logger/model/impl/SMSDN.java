package lk.dialog.ideabiz.logger.model.impl;

import com.google.common.base.CharMatcher;
import lk.dialog.ideabiz.library.NumberFormat;
import lk.dialog.ideabiz.logger.model.LoggerRequest;
import lk.dialog.ideabiz.logger.model.enumarator.SMSType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malinda on 9/9/2015.
 */
public class SMSDN implements LoggerRequest {
    static Integer SMS_UNICODE_LENGTH = 70;
    static Integer SMS_STANDARD_LENGTH = 160;


    String messageStatus = null;
    String messageRef;
    String deliveredTime;

    private static String sqlQuery = "UPDATE `smsmessaging_outbound` SET `messageStatus` = ?,`deleveredTime` = CURRENT_TIMESTAMP()  " +
            "WHERE `messageRef` = ?;";

    @Override
    public PreparedStatement prepareSQL(Connection sqlConnection) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.prepareStatement(sqlQuery);

        preparedStatement.setString(1, this.messageStatus);
        preparedStatement.setString(2, this.messageRef);


        return preparedStatement;

    }

    public SMSDN(String messageStatus, String messageRef, String deliveredTime) {
        this.messageStatus = messageStatus;
        this.messageRef = messageRef;
        this.deliveredTime = deliveredTime;
    }

    public String getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(String messageStatus) {
        this.messageStatus = messageStatus;
    }

    public String getMessageRef() {
        return messageRef;
    }

    public void setMessageRef(String messageRef) {
        this.messageRef = messageRef;
    }

    public String getDeliveredTime() {
        return deliveredTime;
    }

    public void setDeliveredTime(String deliveredTime) {
        this.deliveredTime = deliveredTime;
    }
}
