package lk.dialog.ideabiz.logger;

import com.google.gson.Gson;
import lk.dialog.ideabiz.logger.model.LoggerRequest;
import org.apache.log4j.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Malinda on 9/15/2015.
 */
public class DBRunner extends Thread {
    Connection dbConnection;
    LoggerRequest log;
    Logger logger;
    Boolean bypassLog;
    static Gson gson = new Gson();

    @Override
    public void run() {
        boolean run = false;
        try {
            logger = Logger.getLogger(log.getClass().getCanonicalName());
            PreparedStatement statement = log.prepareSQL(dbConnection);
            statement.executeUpdate();
            run = true;
        } catch (SQLException e) {
            logger.error("Database Error :" + e.getMessage());
        } catch (Exception e) {
            logger.error("Known Error :" + e.getMessage());
        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }

        if (!bypassLog) {
            if (run) {
                logger.info("RECORD:DONE:" + gson.toJson(log));
            } else {
                logger.info("RECORD:ERROR:" + gson.toJson(log));
            }
        }

    }

    public DBRunner(Connection dbConnection, LoggerRequest log, Boolean bypassLog) {
        super();
        this.dbConnection = dbConnection;
        this.log = log;
        this.bypassLog = bypassLog;
    }
}
