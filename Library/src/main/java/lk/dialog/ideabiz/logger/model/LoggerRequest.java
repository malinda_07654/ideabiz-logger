package lk.dialog.ideabiz.logger.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Malinda on 9/9/2015.
 */
public interface LoggerRequest {


    PreparedStatement prepareSQL(Connection sqlConnection) throws SQLException;

}
