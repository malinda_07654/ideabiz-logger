package lk.dialog.ideabiz.logger.model.impl;

import com.google.common.base.CharMatcher;
import lk.dialog.ideabiz.library.NumberFormat;
import lk.dialog.ideabiz.logger.model.LoggerRequest;
import lk.dialog.ideabiz.logger.model.enumarator.SMSType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malinda on 9/9/2015.
 */
public class USSDRequest implements LoggerRequest {
    String version = null;
    Integer appid = null;

    String operator = null;
    String address = null;
    String shortCode = null;
    String keyword = null;
    String clientCorrelator = null;
    String ussdAction = null;
    String sessionId = null;
    String status = null;
    String direction = null;
    SMSType type = null;
    Integer length = null;
    String applicationStatus = null;
    String description = null;
    Long requestId;


    private static String sqlQuery = "INSERT INTO `ussd_request` (`id`,  `datetime`, `unix_time` , `version`, `operator`, `appid`, `address`, `shortCode`, `keyword`, `clientCorrelator`, `ussdAction`, `sessionId`, `status`,`direction`,`type`,`length`, `applicationStatus`,`description`,`requestId`) " +
            "VALUES (NULL, CURRENT_TIMESTAMP(), UNIX_TIMESTAMP (NOW()), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

    @Override
    public PreparedStatement prepareSQL(Connection sqlConnection) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.prepareStatement(sqlQuery);

        formatAddress();

        preparedStatement.setString(1, this.version);
        preparedStatement.setString(2, this.operator);
        preparedStatement.setInt(3, this.appid);
        preparedStatement.setString(4, this.address);
        preparedStatement.setString(5, this.shortCode);
        preparedStatement.setString(6, this.keyword);
        preparedStatement.setString(7, this.clientCorrelator);
        preparedStatement.setString(8, this.ussdAction);
        preparedStatement.setString(9, this.sessionId);
        preparedStatement.setString(10, this.status);
        preparedStatement.setString(11, this.direction);

        if (type == null)
            type = SMSType.STANDARD;

        preparedStatement.setString(12, this.type.toString());
        if (this.length == null)
            this.length = 0;
        preparedStatement.setInt(13, this.length);
        preparedStatement.setString(14, this.applicationStatus);
        preparedStatement.setString(15, this.description);
        if (this.requestId == null)
            this.requestId = 0L;
        preparedStatement.setLong(16, this.requestId);

        return preparedStatement;

    }

    public USSDRequest(String version, String operator, Integer appid, String address, String shortCode, String keyword, String clientCorrelator, String ussdAction, String sessionId, String status, String direction, String type, Integer length, String applicationStatus, String description, Long requestId) {
        this.version = version;
        this.operator = operator;
        this.appid = appid;
        this.address = address;
        this.shortCode = shortCode;
        this.keyword = keyword;
        this.clientCorrelator = clientCorrelator;
        this.ussdAction = ussdAction;
        this.sessionId = sessionId;
        this.status = status;
        this.direction = direction;

        try {
            this.type = SMSType.valueOf(type);
        } catch (Exception e) {
            this.type = SMSType.STANDARD;
        }

        this.length = length;
        this.applicationStatus = applicationStatus;
        this.description = description;
        this.requestId = requestId;
    }

    public USSDRequest(String version, String operator, Integer appid, String address, String shortCode, String keyword, String clientCorrelator, String ussdAction, String sessionId, String status, String direction, String message, String applicationStatus, String description, Long requestId) {
        this.version = version;
        this.operator = operator;
        this.appid = appid;
        this.address = address;
        this.shortCode = shortCode;
        this.keyword = keyword;
        this.clientCorrelator = clientCorrelator;
        this.ussdAction = ussdAction;
        this.sessionId = sessionId;
        this.status = status;
        this.direction = direction;
        this.applicationStatus = applicationStatus;
        this.description = description;
        this.requestId = requestId;

        if (message != null) {
            setType(message);
            this.length = message.length();
        } else {
            this.length = 0;
        }
    }

    public USSDRequest(String version, String operator, Integer appid, String address, String shortCode, String keyword, String clientCorrelator, String ussdAction, String sessionId, String status, String direction, SMSType type, Integer length, String applicationStatus, String description, Long requestId) {
        this.version = version;
        this.operator = operator;
        this.appid = appid;
        this.address = address;
        this.shortCode = shortCode;
        this.keyword = keyword;
        this.clientCorrelator = clientCorrelator;
        this.ussdAction = ussdAction;
        this.sessionId = sessionId;
        this.status = status;
        this.direction = direction;
        this.type = type;
        this.length = length;
        this.applicationStatus = applicationStatus;
        this.description = description;
        this.requestId = requestId;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public SMSType getType() {
        return type;
    }

    public void setType(SMSType type) {
        this.type = type;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getAppid() {
        return appid;
    }

    public void setAppid(Integer appid) {
        this.appid = appid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getClientCorrelator() {
        return clientCorrelator;
    }

    public void setClientCorrelator(String clientCorrelator) {
        this.clientCorrelator = clientCorrelator;
    }

    public String getUssdAction() {
        return ussdAction;
    }

    public void setUssdAction(String ussdAction) {
        this.ussdAction = ussdAction;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public static String getSqlQuery() {
        return sqlQuery;
    }

    public static void setSqlQuery(String sqlQuery) {
        USSDRequest.sqlQuery = sqlQuery;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public void setType(String message) {
        if (CharMatcher.ASCII.matchesAllOf(message)) {
            type = SMSType.STANDARD;
        } else {
            type = SMSType.UNICODE;
        }
    }

    public void formatAddress() {
        if (this.address != null)
            this.address = NumberFormat.formatNumber(this.address);

        if (this.shortCode != null) {
            this.shortCode = this.shortCode.replace("tel:+", "");
            this.shortCode = this.shortCode.replace("tel:", "");
        }
    }


}
