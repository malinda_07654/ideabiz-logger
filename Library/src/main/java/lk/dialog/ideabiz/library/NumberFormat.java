package lk.dialog.ideabiz.library;

import com.google.gson.Gson;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Malinda on 7/14/2015.
 */
public class NumberFormat {
    public static String homeCountryPrefix = "94";
    public static String numberValidatePattern;
    public static int numberValidateMinLength;
    public static int numberValidateMaxLength;

    public static List<String> homeNetworkPrefix;
    static boolean libloaded = false;


    public static String formatNumber(String number) {
        try {
            number = number.replace("tel:+", "");
            number = number.replace("tel:", "");
            number = number.replace("tel", "");

            if (number.startsWith("00"))
                number = number.substring(2);

            if (number.startsWith("0"))
                number = number.substring(1);
            if (number.startsWith("+"))
                number = number.substring(1);
//            if (!number.startsWith("94"))
//                number = homeCountryPrefix + number;
        } catch (Exception e) {

        }


        return number;
    }

    public static boolean validateNumber(String msisdn) {
        Boolean status = true;

        if (numberValidatePattern == null || numberValidatePattern.equals(""))
            return true;


        msisdn = formatNumber(msisdn);

        Pattern pattern = Pattern.compile("\\d{11}");
        Matcher matcher = pattern.matcher(msisdn);


        if (!matcher.matches()) {
            status = false;
        }
        if (msisdn.length() < numberValidateMinLength) {
            status = false;
        }
        if (msisdn.length() > numberValidateMaxLength) {
            status = false;
        }

        return status;

    }

    public static boolean isHomeNetwork(String number) {


        number = formatNumber(number);

        boolean homenet = false;

        for (String prefix : homeNetworkPrefix) {
            if (prefix != null && (!prefix.isEmpty()) && number.startsWith(homeCountryPrefix + prefix)) {
                homenet = true;
                break;
            }
        }

        return homenet;
    }


}
