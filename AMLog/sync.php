<?php
/**
 * Created by IntelliJ IDEA.
 * User: malin
 * Date: 2016-11-08
 * Time: 04:09 PM
 */
include_once 'curl.php';
date_default_timezone_set("Asia/Colombo");

include_once 'conf.php';


$savedData = array(
    array("name" => "apistat", "method" => "amapicalls")
);

makelog("JOB STARTED : " . date("Y-m-d H:i:s", time()));


foreach ($savedData as &$data) {
    $files = glob($log_save_path . $data['name'] . "*.json");

    makelog("READING FILES : " . $data['name'] . "*.json FOUND " . count($files) . " FILES");

    pushMeta($data['name'],"PUSH_STATUS",$amid.",STARTED");
    pushMeta($data['name'],"PUSH_STATUS:".$amid,"STARTED");

    foreach ($files as $file) {
        makelog("PROCESSING FILES : " . $file);
        $up = uploadFile($file, $data['method']);
        if ($up == false) {
            makelog("ERROR PROCESSING FILE : " . $file);

            break;
        }
        makelog("PROCESSING FILES DONE : " . $file);


    }
    makelog("READING FILES : " . $data['name'] . "*.json DONE");

    //push metadata
    if (file_exists($log_work_path . $data['name'])) {
        $last_exe_time = file_get_contents($log_work_path . $data['name']);

        pushMeta($data['name'],"LAST_EXE_GREP",$last_exe_time);
        pushMeta($data['name'],"LAST_RUN_TIME",date("Y-m-d H:i:s", time()));
        pushMeta($data['name'],"LAST_AM",$amid);
        pushMeta($data['name'],"LAST_SYNC_GAP",$timegap*60);

        pushMeta($data['name'],"LAST_EXE_GREP:".$amid,$last_exe_time);
        pushMeta($data['name'],"LAST_RUN_TIME:".$amid,date("Y-m-d H:i:s", time()));
        pushMeta($data['name'],"LAST_SYNC_GAP:".$amid,$timegap*60);
    }

    pushMeta($data['name'],"PUSH_STATUS",$amid.",DONE");
    pushMeta($data['name'],"PUSH_STATUS:".$amid,"DONE");

}

makelog("JOB DONE ");


function uploadFile($file, $method)
{
    global $push_delay;

    $content = file_get_contents($file);
    $lines = explode(PHP_EOL, $content);
    makelog("PROCESSING LINES : " . count($lines));
    foreach ($lines as $line) {
        if (strlen($line) > 2) {
            $push = sendPush($line, $method);
            if ($push == false)
                return false;

            sleep($push_delay);
        }

    }
    makelog("RENAME LINES : " . $file . " to " . $file . ".completed");

    //rename($file, $file . ".completed");
    unlink($file);
    return true;

}

function sendPush($line, $method)
{
    global $log_url;
    $result = getHTTP($log_url . $method, $line, "POST", null, array("Content-Type: application/json"), null, true);

    makelog("SEND PUSH : " . $log_url . $method . " | " . $line . " | " . $result['status'] . " | " . $result['statusCode']);

    if ($result['status'] != "OK" || $result['statusCode'] != "200")
        return false;

    return true;
}

function pushMeta($logger, $key,$value)
{
    global $log_url;
    $metadata = array("LOGER_NAME"=>$logger,"KEY"=>$key,"VALUE"=>$value);
    $result = getHTTP($log_url . "logermeta", json_encode($metadata), "POST", null, array("Content-Type: application/json"), null, true);
    makelog("SEND META : " . $log_url . "logermeta" . " | " . json_encode($metadata) . " | " . $result['status'] . " | " . $result['statusCode']);

}


function makelog($string)
{
    global $log_work_path;
    echo date("Y-m-d H:i:s", time()) . " : " . $string . PHP_EOL;
    file_put_contents($log_work_path . "sync.log.".date("Y-m-d", time()), date("Y-m-d H:i:s", time()) . " : " . $string . PHP_EOL, FILE_APPEND);
}