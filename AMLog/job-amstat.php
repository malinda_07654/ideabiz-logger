<?php
include_once 'conf.php';
$name = "apistat";

begin:


$exe_time = null;
$exe_file = null;
$time_save = null;

date_default_timezone_set("Asia/Colombo");

$time_start = time();
$time_exec = null;
makelog("JOB STARTED : " . date("Y-m-d H:i:s", time()));

if (file_exists($log_work_path . $name)) {
    $last_exe_time = file_get_contents($log_work_path . $name);
    $currentDate = strtotime($last_exe_time);
    $readTime = $currentDate + (60 * $timegap); //10min

    makelog("READING LAST FROM FILE " . $last_exe_time);
    if ($readTime > time() - (60 * $timegap)) {
        //need minimum 10min gap
        makelog("READING LAST FROM NOT ENOUGH GAP TO CURRENT TIME " .(time() - (60 * $timegap))."|". $last_exe_time." | ".$readTime."|".(time() - (60 * $timegap)));
        exit;
    }
    $time_save = date("Y-m-d H:i:s", $readTime);
} else {
    $time_save = date("Y-m-d H:i:s", time() - (60 * $timegap));
}


$exe_date = date("Y-m-d", strtotime($time_save));
$exe_time = substr(date("Y-m-d H:i", strtotime($time_save)), 0, -1);
//$exe_file ="apistat-2017-01-31_111124";
$exe_file = $name . "-" . date("Y-m-d_His", strtotime($time_save));

makelog("SAVE TIME " . $time_save . " GREP " . $exe_time . " FILE " . $exe_file . " OUTPUT " . $log_work_path . $exe_file . ".out");


$command = "grep '" . $exe_time . "' " . $log_file_path . "wso2carbon.log |grep '>>>>>' | awk -F' ' '{print $5\",\"\$15}'  | gawk 'match($0, /(.*)([0-9]*)MI([0-9]*)([a-zA-Z_]*)([0-9])/, a) {print a[1]\",\"a[3]\",\"a[4]}'  | awk -F',' '{print \$1\",\"\$5\",\"\$4}'  | sort | uniq -c | sort -n > " . $log_work_path . $exe_file . ".out";
makelog("COMMAND : " . $command);
$out = shell_exec ($command);
//makelog("COMMAND OUT : ".$out);
$time_exec = time();
$file = file_get_contents($log_work_path . $exe_file . ".out");
//$file = file_get_contents("apistat-2016-11-06_164833.out");

$lines = explode("\n", $file);
makelog("LINES " . count($lines));

foreach ($lines as &$line) {

    try {
        $apiInfo = array();

        $line = trim($line);

        //"\d+\s\d+:\d+:\d+,\w+,\d+" == 1 16:40:03,payment,190
        if (!preg_match('/\d+\s\d+:\d+:\d+,\w+,\d+/', $line)) {
            continue;
        }
        $v = explode(' ', $line, 2);
        $apiInfo['count'] = $v[0];
        $line = $v[1];

        $v = explode(",", $line);
        $apiInfo['datetime'] = $exe_date . " " . $v[0];
        $apiInfo['API'] = $v[1];
        $apiInfo['appid'] = $v[2];
        $apiInfo['version'] = "";
        $apiInfo['AM'] = $amid;
        file_put_contents($log_save_path . $exe_file . ".json", json_encode($apiInfo).PHP_EOL, FILE_APPEND);
        makelog("OUT : " . json_encode($apiInfo));
    } catch (Exception $e) {
        makelog("ERROR : " . $e);
    }
}

unlink($log_work_path . $exe_file . ".out");
file_put_contents($log_work_path . $name, $time_save);
makelog("JOB FINISHED : TOTAL - " . (time() - $time_start) . "s, COMMAND - " . ($time_exec - $time_start) . "s");
makelog("JOB FINISHED : " . date("Y-m-d H:i:s", time()));

if((time() - (60 * 19)) > strtotime($time_save)){
	makelog("HAVE PENDING. GOTO BEGIN : " .(time() - (60 * 19)) .":".strtotime($time_save));
	goto begin ;
}

function makelog($string)
{
    global $log_work_path, $name;
    echo date("Y-m-d H:i:s", time()) . " : " . $string . PHP_EOL;
    file_put_contents($log_work_path . $name . ".log.".date("Y-m-d", time()), date("Y-m-d H:i:s", time()) . " : " . $string . PHP_EOL, FILE_APPEND);
}