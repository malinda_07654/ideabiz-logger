#Modules

## Library
Maven library that can import to any project. In lirary, user need to create own table structure. and need impliment `LoggerRequest`

`prepareSQL` method  need to return sql statement


Sample Implimentation
```
public class BalanceCheck implements LoggerRequest {
    String version = null;
    Integer appid = null;

    String address = null;
    String status = null;
    Long CG_Time;
    Long total_time;



    private static String sqlQuery = "INSERT INTO `payment_balancecheck` (`id`,  `datetime`, `unix_time` , `version`, `appid`, `address`, `status`,`CG_time`,`total_time`) " +
            "VALUES (NULL, CURRENT_TIMESTAMP(), UNIX_TIMESTAMP (NOW()), ?, ?, ?, ?, ?, ?);";

    @Override
    public PreparedStatement prepareSQL(Connection sqlConnection) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.prepareStatement(sqlQuery);
        formatAddress();
        preparedStatement.setString(1, this.version);
        preparedStatement.setInt(2, this.appid);
        preparedStatement.setString(3, this.address);
        preparedStatement.setString(4, this.status);
        if (this.CG_Time == null)
            CG_Time = new Long(0);
        preparedStatement.setLong(5, this.CG_Time);
        if (this.total_time == null)
            total_time = new Long(0);
        preparedStatement.setLong(6, this.total_time);

        return preparedStatement;
    }

    public BalanceCheck(String version, Integer appid, String address, String status, Long CGTime, Long totalTime) {
        this.version = version;
        this.appid = appid;
        this.address = address;
        this.status = status;
        this.CG_Time = CGTime;
        this.total_time = totalTime;
    }
}
```

In code, user need to create DirectLogger Object
When creating directLogger, user can send database connection as parameter. or user can just create object without params. 
If no params, it will use `conf.properties` file in app resources. (if no `conf.properties` file found in app resources, it will looking for logger library resources).

eg:
```
  directLogger = new DirectLogger(databaseLibrary.getDatabaseSource());
```

```
  directLogger = new DirectLogger();
```

Then user need to call 
```
  directLogger.addLog(digitalRewards);
```

`addLog` will run the async db call and record the log in DB. `addLog` accept implimented class of `LoggerRequest`.


## Mediator

This is mediator for APIManager that record API call logs async  for reporting. can use this for thirdprty API's

* Library need to add as module (Optional)
* Library need to Maven Install before run this


## Web Service
this if  need run logger as webservice. so thirdparty can send and report their apicall stats using webservice

* Library need to add as module (Optional)
* Library need to Maven Install before run this


#Missing Logs inserting

For charging 

* run bellow bash on payment adaptor log folder.

This is for one month
```
cat payment.log.2016-04-* | grep "RECORD:ERROR:" > ~/error-payment-201604.log

```
This will filter all error logging record and save it in to `error-payment-201604.log` file

* Then run webservice  with local DB
* Clear all webservice logs (log4j file) and localDB
* Open `error-payment-201604.log` and make API call with file body
* call `http://localhost/log/payment/log` method as post with above file content
	This method will split post body by line by line. then read the json and add to the DB
* Onece finish inserting, please check webservice log file.
* If no error, and all inserted. connect to the live DB. and run

