package lk.dialog.ideabiz.logger.handler;

import com.google.gson.Gson;
import lk.dialog.ideabiz.library.JWT;
import lk.dialog.ideabiz.logger.DirectLogger;
import lk.dialog.ideabiz.logger.model.impl.DigitalRewards;
import lk.dialog.ideabiz.model.DigitalRewards.*;
import lk.dialog.ideabiz.model.JWT.JWTTokenInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.ManagedLifecycle;
import org.apache.synapse.Mediator;
import org.apache.synapse.MessageContext;
import org.apache.synapse.SynapseConstants;
import org.apache.synapse.commons.json.JsonUtil;
import org.apache.synapse.core.SynapseEnvironment;
import org.apache.synapse.core.axis2.Axis2MessageContext;
import org.apache.synapse.rest.AbstractHandler;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Malinda on 9/17/2015.
 */
public class DigitalRewardsHandler extends AbstractHandler implements ManagedLifecycle {
    private static final Log log = LogFactory.getLog(DigitalRewardsHandler.class);
    private static DirectLogger directLogger = null;
    Gson gson = null;

    public boolean handleRequest(MessageContext messageContext) {
        try {

            Axis2MessageContext axis2MessageContext = (Axis2MessageContext) messageContext;
            String resourceUrl = (String) messageContext.getProperty("REST_FULL_REQUEST_PATH");

            Mediator sequence = messageContext.getSequence("_build_");
            sequence.mediate(messageContext);
            String jsonPayloadToString = JsonUtil.jsonPayloadToString(((Axis2MessageContext) messageContext).getAxis2MessageContext());

            String appID = messageContext.getProperty("api.ut.application.id").toString();
            Integer appIdInt = 0;

            log.info("Digital rewards request |" + resourceUrl + "|");

            try {
                appIdInt = Integer.parseInt(appID);
            } catch (Exception ex) {

            }

            if (resourceUrl.endsWith("generate")) {
                log.info("Digital Rewards Generate Request : " + jsonPayloadToString);
                RewardsGenerateRequestWrap rewardsGenerateRequestWrap = gson.fromJson(jsonPayloadToString, RewardsGenerateRequestWrap.class);
                //.*rewards/(.+?)/generate
                String rgx = ".*rewards/(.+?)/generate";
                Pattern pattern = Pattern.compile(rgx);
                Matcher matcher = pattern.matcher(resourceUrl);
                matcher.find();
                String type = (matcher.group(1));
                directLogger.addLog(new DigitalRewards("v0.1", appIdInt, type, "generate", rewardsGenerateRequestWrap.getRewardsGenerateRequest().getAmount(), null, rewardsGenerateRequestWrap.getRewardsGenerateRequest().getClientCorrelator(), rewardsGenerateRequestWrap.getRewardsGenerateRequest().getRewardsTag(), "", "", ""));
            } else if (resourceUrl.endsWith("apply")) {
                log.info("Digital Rewards Apply Request : " + jsonPayloadToString);
                //.*rewards/(.+?)/generate
                String rgx = ".*rewards/(.+?)/apply";
                Pattern pattern = Pattern.compile(rgx);
                Matcher matcher = pattern.matcher(resourceUrl);
                matcher.find();
                String msisdn = (matcher.group(1));

                RewardsApplyRequestWrap rewardsApplyRequestWrap = gson.fromJson(jsonPayloadToString, RewardsApplyRequestWrap.class);
                directLogger.addLog(new DigitalRewards("v0.1", appIdInt, rewardsApplyRequestWrap.getRewardsApplyRequest().getType(), "apply", Double.valueOf(rewardsApplyRequestWrap.getRewardsApplyRequest().getAmount()), msisdn, rewardsApplyRequestWrap.getRewardsApplyRequest().getClientCorrelator(), rewardsApplyRequestWrap.getRewardsApplyRequest().getRewardsTag(), "", rewardsApplyRequestWrap.getRewardsApplyRequest().getSmsText(), ""));
            } else {
                log.info("Digital Rewards Redeem Request : " + jsonPayloadToString);
                String rgx = ".*/rewards/(.+?)$";
                Pattern pattern = Pattern.compile(rgx);
                Matcher matcher = pattern.matcher(resourceUrl);
                matcher.find();
                String msisdn = (matcher.group(1));
                if (msisdn != null)
                    msisdn = msisdn.replace("/", "");
                RewardsRedeemRequestWrap rewardsRedeemRequestWrap = gson.fromJson(jsonPayloadToString, RewardsRedeemRequestWrap.class);
                directLogger.addLog(new DigitalRewards("v0.1", appIdInt, "", "redeem", 0.0, msisdn, rewardsRedeemRequestWrap.getRewardsRedeemRequest().getClientCorrelator(), "", rewardsRedeemRequestWrap.getRewardsRedeemRequest().getPin(), "", ""));
            }

        } catch (Exception e) {
            log.error("Digital Reward Request Error : " + e.getMessage(), e);
        }

        return true;
    }

    public boolean handleResponse(MessageContext messageContext) {
        try {
            log.info("Digital Rewards Response " + "|");
            Axis2MessageContext axis2MessageContext = (Axis2MessageContext) messageContext;


        } catch (Exception e) {
            log.error("Digital Rewards response :" + e.getMessage(), e);
            String s = gson.toJson(new DigitalRewardsException("ERROR", ""));
            JsonUtil.newJsonPayload(((Axis2MessageContext) messageContext).getAxis2MessageContext(), s, true, true);
        }

        return true;
    }


    public DigitalRewardsHandler() {
        gson = new Gson();
        directLogger = new DirectLogger();
        log.info("Digital reward Constructor|");

    }

    public void init(SynapseEnvironment synapseEnvironment) {
        log.info("Digital reward Init");

    }

    public void destroy() {
        log.info("Digital reward Init|");

    }

}
