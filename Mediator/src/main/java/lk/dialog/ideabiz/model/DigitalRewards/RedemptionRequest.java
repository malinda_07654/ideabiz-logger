package lk.dialog.ideabiz.model.DigitalRewards;

/**
 * Created by Malinda on 9/17/2015.
 */
public class RedemptionRequest {
    String notifyURL;
    String callbackData;

    public String getNotifyURL() {
        return notifyURL;
    }

    public void setNotifyURL(String notifyURL) {
        this.notifyURL = notifyURL;
    }

    public String getCallbackData() {
        return callbackData;
    }

    public void setCallbackData(String callbackData) {
        this.callbackData = callbackData;
    }
}
