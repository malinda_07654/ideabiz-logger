package lk.dialog.ideabiz.model.DigitalRewards;

/**
 * Created by Malinda on 9/17/2015.
 */
public class RewardsApplyRequest {
    String type;
    String amount;
    String clientCorrelator;
    RedemptionRequest redemptionRequest;
    String rewardsTag;
    String smsText;

    public RewardsApplyRequest() {
        redemptionRequest = new RedemptionRequest();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getClientCorrelator() {
        return clientCorrelator;
    }

    public void setClientCorrelator(String clientCorrelator) {
        this.clientCorrelator = clientCorrelator;
    }

    public RedemptionRequest getRedemptionRequest() {
        return redemptionRequest;
    }

    public void setRedemptionRequest(RedemptionRequest redemptionRequest) {
        this.redemptionRequest = redemptionRequest;
    }

    public String getRewardsTag() {
        return rewardsTag;
    }

    public void setRewardsTag(String rewardsTag) {
        this.rewardsTag = rewardsTag;
    }

    public String getSmsText() {
        return smsText;
    }

    public void setSmsText(String smsText) {
        this.smsText = smsText;
    }
}
