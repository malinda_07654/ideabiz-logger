package lk.dialog.ideabiz.model.DigitalRewards;

/**
 * Created by Malinda on 9/18/2015.
 */
public class RewardsRedeemResponse {
    String pin;
    String clientCorrelator;
    RedemptionRequest redemptionRequest;
    Double amount;
    String type;
    String resourceURL;

    public RewardsRedeemResponse() {
        redemptionRequest=new RedemptionRequest();
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getClientCorrelator() {
        return clientCorrelator;
    }

    public void setClientCorrelator(String clientCorrelator) {
        this.clientCorrelator = clientCorrelator;
    }

    public RedemptionRequest getRedemptionRequest() {
        return redemptionRequest;
    }

    public void setRedemptionRequest(RedemptionRequest redemptionRequest) {
        this.redemptionRequest = redemptionRequest;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getResourceURL() {
        return resourceURL;
    }

    public void setResourceURL(String resourceURL) {
        this.resourceURL = resourceURL;
    }
}
