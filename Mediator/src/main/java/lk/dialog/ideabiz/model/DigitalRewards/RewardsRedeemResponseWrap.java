package lk.dialog.ideabiz.model.DigitalRewards;

/**
 * Created by Malinda on 9/18/2015.
 */
public class RewardsRedeemResponseWrap {
    RewardsRedeemResponse rewardsRedeemResponse;

    public RewardsRedeemResponse getRewardsRedeemResponse() {
        return rewardsRedeemResponse;
    }

    public void setRewardsRedeemResponse(RewardsRedeemResponse rewardsRedeemResponse) {
        this.rewardsRedeemResponse = rewardsRedeemResponse;
    }
}
