package lk.dialog.ideabiz.model.DigitalRewards;

/**
 * Created by Malinda on 9/18/2015.
 */
public class RewardsGenerateResponseWrap {
    RewardsGenerateResponse rewardsGenerateResponse;

    public RewardsGenerateResponse getRewardsGenerateResponse() {
        return rewardsGenerateResponse;
    }

    public void setRewardsGenerateResponse(RewardsGenerateResponse rewardsGenerateResponse) {
        this.rewardsGenerateResponse = rewardsGenerateResponse;
    }
}
