package lk.dialog.ideabiz.model.DigitalRewards;

/**
 * Created by Malinda on 9/19/2015.
 */
public class RewardsGenerateRequestWrap {
    RewardsGenerateRequest rewardsGenerateRequest = null;

    public RewardsGenerateRequest getRewardsGenerateRequest() {
        return rewardsGenerateRequest;
    }

    public void setRewardsGenerateRequest(RewardsGenerateRequest rewardsGenerateRequest) {
        this.rewardsGenerateRequest = rewardsGenerateRequest;
    }
}
