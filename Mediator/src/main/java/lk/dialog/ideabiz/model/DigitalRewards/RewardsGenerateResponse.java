package lk.dialog.ideabiz.model.DigitalRewards;

/**
 * Created by Malinda on 9/18/2015.
 */
public class RewardsGenerateResponse {
    Double amount;
    String pin;
    String clientCorrelator;
    RedemptionRequest redemptionRequest;
    String rewardsTag;

    public RewardsGenerateResponse() {
        redemptionRequest = new RedemptionRequest();
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getClientCorrelator() {
        return clientCorrelator;
    }

    public void setClientCorrelator(String clientCorrelator) {
        this.clientCorrelator = clientCorrelator;
    }

    public RedemptionRequest getRedemptionRequest() {
        return redemptionRequest;
    }

    public void setRedemptionRequest(RedemptionRequest redemptionRequest) {
        this.redemptionRequest = redemptionRequest;
    }

    public String getRewardsTag() {
        return rewardsTag;
    }

    public void setRewardsTag(String rewardsTag) {
        this.rewardsTag = rewardsTag;
    }
}
