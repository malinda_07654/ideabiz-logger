package lk.dialog.ideabiz.model.DigitalRewards;

/**
 * Created by Malinda on 9/17/2015.
 */
public class RewardsApplyRequestWrap {
    RewardsApplyRequest rewardsApplyRequest;

    public RewardsApplyRequest getRewardsApplyRequest() {
        return rewardsApplyRequest;
    }

    public void setRewardsApplyRequest(RewardsApplyRequest rewardsApplyRequest) {
        this.rewardsApplyRequest = rewardsApplyRequest;
    }
}
