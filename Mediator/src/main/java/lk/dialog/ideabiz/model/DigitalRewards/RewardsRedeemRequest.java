package lk.dialog.ideabiz.model.DigitalRewards;

/**
 * Created by Malinda on 9/18/2015.
 */
public class RewardsRedeemRequest {
    String pin;
    String clientCorrelator;
    RedemptionRequest redemptionRequest;

    public RewardsRedeemRequest() {
        redemptionRequest=new RedemptionRequest();
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getClientCorrelator() {
        return clientCorrelator;
    }

    public void setClientCorrelator(String clientCorrelator) {
        this.clientCorrelator = clientCorrelator;
    }

    public RedemptionRequest getRedemptionRequest() {
        return redemptionRequest;
    }

    public void setRedemptionRequest(RedemptionRequest redemptionRequest) {
        this.redemptionRequest = redemptionRequest;
    }
}
