package lk.dialog.ideabiz.model.DigitalRewards;

/**
 * Created by Malinda on 9/18/2015.
 */
public class RewardsRedeemRequestWrap {
    RewardsRedeemRequest rewardsRedeemRequest;

    public RewardsRedeemRequest getRewardsRedeemRequest() {
        return rewardsRedeemRequest;
    }

    public void setRewardsRedeemRequest(RewardsRedeemRequest rewardsRedeemRequest) {
        this.rewardsRedeemRequest = rewardsRedeemRequest;
    }
}
