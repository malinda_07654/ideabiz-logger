package lk.dialog.ideabiz.model.DigitalRewards;

/**
 * Created by Malinda on 9/19/2015.
 */
public class DigitalRewardsException {
    String Status =null;
    String message =null;

    public DigitalRewardsException(String status, String message) {
        Status = status;
        this.message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
