<?php
/**
 * Created by IntelliJ IDEA.
 * User: malin
 * Date: 2016-11-09
 * Time: 09:40 AM
 */
$conf_log_dbname = 'ideabizlog';
$conf_log_dbuser = 'root';
$conf_log_dbpw = 'dialog@123';
$conf_log_dbhost = 'localhost';
$conf_log_dbhost = '172.26.75.104';

$conf_AM_dbname = 'dbApiMgt';
$conf_AM_dbuser = 'root';
$conf_AM_dbpw = 'dialog@123';
$conf_AM_dbhost = 'localhost';
$conf_AM_dbhost = '172.26.75.103';

$conf_ADMIN_dbname = 'ideabizadmin';
$conf_ADMIN_dbuser = 'root';
$conf_ADMIN_dbpw = 'dialog@123';
$conf_ADMIN_dbhost = 'localhost';
$conf_ADMIN_dbhost = '172.26.75.103';

$graphAPI = array("BalanceCheck", "payment", "smsmessaging", "IdeabizSubscription", "DigitalRewards", "StarPoint");

$LOGdb = mysql_connect($conf_log_dbhost, $conf_log_dbuser, $conf_log_dbpw);
mysql_select_db($conf_log_dbname, $LOGdb);

$AMdb = mysql_connect($conf_AM_dbhost, $conf_AM_dbuser, $conf_AM_dbpw);
mysql_select_db($conf_AM_dbname, $AMdb);

$ADMINdb = mysql_connect($conf_ADMIN_dbhost, $conf_ADMIN_dbuser, $conf_ADMIN_dbpw);
mysql_select_db($conf_ADMIN_dbname, $ADMINdb);


date_default_timezone_set('Asia/Colombo');
$str = "SET time_zone = '+5:30';";
$result = mysql_query($str);

$daily = getDailyStat();
$monthly = getMonthlyStats();
$total12 = get12Month($monthly);
$total30 = ($monthly['last']['total']);

//24hr
makelog("GETTING API CALLS INFO");

$apicallsSummery = getAPICallsSummeryWithCache();
$apicallGraph = getAPIGraphTimeWithCache();
$apicallTotal24 = getAPICalltotal24();
$apicallTotalMonth = getAPICalltotalMonth();

makelog("GETTING AMAPPS CALLS INFO");
$AMApps = getAMTotalAMApps();
$AMSP = getAMSubscribers();
$AMAPI = getAMAPICount();

makelog("GETTING SUBS");

$Sub = getSUB();


$result = array(
    //daily stat last 30day and today
    "daily" => $daily,
    //monthly stat, last 12 month and this month
    "monthly" => $monthly,
    "last12MonthTotal" => $total12,
    "last30DayTotal" => $total30,
    //API call summery for last 24hr total API calls per API for donut
    "APICallsSummery" => $apicallsSummery,
    //selected API 10min group API call count
    "APICallsGraph" => $apicallGraph,
    "APICallsLast24hr" => $apicallTotal24,
    "APICallsLastMonth" => $apicallTotalMonth,
    //AM App summery. total apps, total sp, totalAPIs
    "AM" => array("apps" => $AMApps, "sp" => $AMSP, "API" => $AMAPI),
    //subscription summery for last 7days and total user base
    "subs" => $Sub
);

file_put_contents("dashAPI-summery-cache.json", json_encode($result));
echo json_encode($result);

function getSUB()
{
    $daily = array('active' => null, "history" => array(), 'updated' => null);

    if (file_exists('subs-history.json')) {
        $daily = json_decode(file_get_contents("daily.json"), true);
    }


    if ($daily['updated'] == null || $daily['updated'] < (time() - (10 * 60))) {

        $daily['active'] = getTotalSubs(date('Y-m-d', strtotime("-12Months")), date('Y-m-d', time()), "SUBSCRIBED");
        $daily['history'] = array();
        $daily['history'][date('Y-m-d', time())] = getSubscriptionHistory(date('Y-m-d', time()), date('Y-m-d', time()), "SUBSCRIBED");
        $daily['history'][date('Y-m-d', strtotime("-1Days"))] = getSubscriptionHistory(date('Y-m-d', strtotime("-1Days")), date('Y-m-d', strtotime("-1Days")), "SUBSCRIBED");
        $daily['history'][date('Y-m-d', strtotime("-2Days"))] = getSubscriptionHistory(date('Y-m-d', strtotime("-2Days")), date('Y-m-d', strtotime("-2Days")), "SUBSCRIBED");
        $daily['history'][date('Y-m-d', strtotime("-3Days"))] = getSubscriptionHistory(date('Y-m-d', strtotime("-3Days")), date('Y-m-d', strtotime("-3Days")), "SUBSCRIBED");
        $daily['history'][date('Y-m-d', strtotime("-4Days"))] = getSubscriptionHistory(date('Y-m-d', strtotime("-4Days")), date('Y-m-d', strtotime("-4Days")), "SUBSCRIBED");
        $daily['history'][date('Y-m-d', strtotime("-5Days"))] = getSubscriptionHistory(date('Y-m-d', strtotime("-5Days")), date('Y-m-d', strtotime("-5Days")), "SUBSCRIBED");
        $daily['history'][date('Y-m-d', strtotime("-6Days"))] = getSubscriptionHistory(date('Y-m-d', strtotime("-6Days")), date('Y-m-d', strtotime("-6Days")), "SUBSCRIBED");
        $daily['history'][date('Y-m-d', strtotime("-7Days"))] = getSubscriptionHistory(date('Y-m-d', strtotime("-7Days")), date('Y-m-d', strtotime("-7Days")), "SUBSCRIBED");
        $daily['updated'] = time();
        file_put_contents('subs-history.json', json_encode($daily));
    }
    return $daily;
}

function getDailyStat()
{
    $daily = array('last' => array('date' => date('Y-m-d', time()), "total" => null), "history" => array(), 'updated' => null);


    if (file_exists('daily.json')) {
        $daily = json_decode(file_get_contents("daily.json"), true);
    }


    if ($daily['updated'] == null || $daily['updated'] < (time() - (5 * 60))) {
        $daily['last']['date'] = date('Y-m-d', time());
        $daily['last']['total'] = getRevenue(date('Y-m-d', time()), date('Y-m-d', time()));
        $daily['updated'] = time();
    }

    $lastHistory = null;
    if (count($daily['history']) == 0) {
        $lastHistory = date('Y-m-d', strtotime("-31Days"));
    } else {
        $lastHistory = date('Y-m-d', strtotime($daily['history'][count($daily['history']) - 1]['date'] . " +1day"));
    }

    while ($lastHistory != date('Y-m-d', time())) {
        $d = getRevenue($lastHistory, $lastHistory);
        array_push($daily['history'], array('date' => $lastHistory, "total" => $d));


        if (count($daily['history']) > 30) {
            array_shift($daily['history']);
        }
        $lastHistory = date('Y-m-d', strtotime($lastHistory . " +1Day"));
    }


    file_put_contents("daily.json", json_encode($daily));
    return $daily;
}

function get12Month($monthly)
{
    $sum = $monthly['last']['total'];
    foreach ($monthly['history'] as $month) {
        $sum += $month['total'];
    }

    return $sum;
}

function getMonthlyStats()
{
    $monthly = array('last' => array('date' => date('Y-m', time()), "total" => null), 'updated' => null, "history" => array());


    if (file_exists('monthly.json')) {
        $monthly = json_decode(file_get_contents("monthly.json"), true);
    }


    if ($monthly['updated'] == null || $monthly['updated'] < (time() - (60 * 60))) {
        $monthly['last']['month'] = date('Y-m', time());
        $monthly['last']['total'] = getRevenue(date('Y-m', time()) . "-01", date('Y-m-d', time()));
        $monthly['updated'] = time();
    }

    $lastHistory = null;
    if (count($monthly['history']) == 0) {
        $lastHistory = date('Y-m', strtotime("-12Months"));
    } else {
        $lastHistory = date('Y-m', strtotime($monthly['history'][count($monthly['history']) - 1]['date'] . " +1Month"));
    }

    while ($lastHistory != date('Y-m', time())) {
        $d = getRevenue($lastHistory . "-01", $lastHistory . "-31");
        array_push($monthly['history'], array('date' => $lastHistory, "total" => $d));


        if (count($monthly['history']) > 12) {
            array_shift($monthly['history']);
        }
        $lastHistory = date('Y-m', strtotime($lastHistory . " +1Month"));
    }


    file_put_contents("monthly.json", json_encode($monthly));
    return $monthly;
}

function getRevenue($from, $to)
{
    global $LOGdb;

    makelog("GETTING REVENUE : " . $from . " to " . $to);
    $s = 'SELECT sum(amount) as amount FROM payment_transaction WHERE datetime >="' . $from . ' 00:00:00" AND datetime <="' . $to . ' 23:59:59" AND result_status = "SUCCESS"';
    $result = mysql_query($s, $LOGdb);
    if ($result == 0)
        return 0;
    $num = mysql_numrows($result);

    if ($num == 0)
        return 0;
    if (mysql_result($result, 0, "amount") == null)
        return 0;
    else
        return (mysql_result($result, 0, "amount"));
}


function getAPICallsSummeryWithCache()
{

    $APIcalls = array('data' => array(), 'updated' => null);


    if (file_exists('APICalls.json')) {
        $APIcalls = json_decode(file_get_contents("APICalls.json"), true);
    }


    if ($APIcalls['updated'] == null || $APIcalls['updated'] < (time() - (5 * 60))) {

        $APIcalls['data'] = getAPICallsSummery(date('Y-m-d H:i:s', strtotime("-24hour")), date('Y-m-d H:i:s', time()));
        $APIcalls['updated'] = time();

        file_put_contents('APICalls.json', json_encode($APIcalls));

    }

    return $APIcalls;
}

function getAPICalltotal24()
{

    $APIcalls = array('total' => array(), 'updated' => null);


    if (file_exists('APICallsTotal24.json')) {
        $APIcalls = json_decode(file_get_contents("APICallsTotal24.json"), true);
    }


    if ($APIcalls['updated'] == null || $APIcalls['updated'] < (time() - (10 * 60))) {

        $APIcalls['total'] =getAPICallsTotal(date('Y-m-d H:i:s', strtotime("-24hour")), date('Y-m-d H:i:s', time()));
        $APIcalls['updated'] = time();

        file_put_contents('APICallsTotal24.json', json_encode($APIcalls));

    }

    return $APIcalls;
}

function getAPICalltotalMonth()
{

    $APIcalls = array('total' => array(), 'updated' => null);


    if (file_exists('APICallsTotalMonth.json')) {
        $APIcalls = json_decode(file_get_contents("APICallsTotalMonth.json"), true);
    }


    if ($APIcalls['updated'] == null || $APIcalls['updated'] < (time() - (30 * 60))) {

        $APIcalls['total'] =getAPICallsTotal(date('Y-m-d H:i:s', strtotime("-30Days")), date('Y-m-d H:i:s', time()));
        $APIcalls['updated'] = time();

        file_put_contents('APICallsTotalMonth.json', json_encode($APIcalls));

    }

    return $APIcalls;
}

function getAPIGraphTimeWithCache()
{
    global $graphAPI;

    $APIcalls = array('data' => array(), 'updated' => null);


    if (file_exists('APIGraph.json')) {
        $APIcalls = json_decode(file_get_contents("APIGraph.json"), true);
    }


    if ($APIcalls['updated'] == null || $APIcalls['updated'] < (time() - (5 * 60))) {
        $APIcalls = array('data'=>array(),'updated'=>null);
        foreach ($graphAPI as $API) {
            $d = array("API" => $API, "data" => getAPICalls(date('Y-m-d H:i:s', strtotime("-24hour")), date('Y-m-d H:i:s', time()), $API));

            array_push($APIcalls['data'], $d);
        }

        $APIcalls['updated'] = time();
        file_put_contents('APIGraph.json', json_encode($APIcalls));

    }

    return $APIcalls;
}

function getAPICallsSummery($from, $to)
{
    global $LOGdb;
    makelog("GETTING API Summery : " . $from . " to " . $to);

    $s = 'SELECT API,sum(`count`) as c FROM am_apicalls WHERE  datetime >="' . $from . '" AND datetime <="' . $to . '"  GROUP BY API;';
    $result = mysql_query($s, $LOGdb);
    $num = mysql_numrows($result);

    $arr = array();
    $i = 0;
    while ($i < $num) {
        $d = array("API" => (mysql_result($result, $i, "API")), "total" => (mysql_result($result, $i, "c")));
        array_push($arr, $d);
        $i++;
    }

    return $arr;

}


function getAPICallsTotal($from, $to)
{
    global $LOGdb;
    makelog("GETTING API Total : " . $from . " to " . $to);

    $s = 'SELECT sum(`count`) as c FROM am_apicalls WHERE  datetime >="' . $from . '" AND datetime <="' . $to . '" ;';
    $result = mysql_query($s, $LOGdb);
    $num = mysql_numrows($result);

    if ($result == 0)
        return 0;
    $num = mysql_numrows($result);

    if ($num == 0)
        return 0;
    if (mysql_result($result, 0, "c") == null)
        return 0;
    else
        return (mysql_result($result, 0, "c"));

}

function getAPICalls($from, $to, $API)
{
    global $LOGdb;
    makelog("GETTING API INFO : " . $from . " to " . $to);


    $s = 'SELECT substring(`datetime`,1,15) as dt, api,sum(`count`) as c FROM am_apicalls WHERE  datetime >="' . $from . '" AND datetime <="' . $to . '"   AND `API` = "' . $API . '" GROUP BY  substring(`datetime`,1,15), API;';
    $result = mysql_query($s, $LOGdb);
    $num = mysql_numrows($result);

    $arr = array();
    $c =substr(date('Y-m-d H:i', strtotime( $from)),0,-1)."0:00";
    while (strtotime($c) <= strtotime($to)) {
        array_push($arr, array("datetime" => $c, "total" => 0));
        $c = date('Y-m-d H:i:s', strtotime($c . " +10min"));
    }

    $i = 0;
    while ($i < $num) {
        $d = array("datetime" => (mysql_result($result, $i, "dt")) . "0:00", "total" => (mysql_result($result, $i, "c")));
        for ($x = 0; $x < count($arr); $x++) {
            if ($arr[$x]['datetime'] == $d['datetime']) {
                $arr[$x]['total'] = $d['total'];
                break;
            }
        }
        $i++;
    }

    return $arr;

}

function makelog($string, $debug = false)
{

    file_put_contents("api.out", date('Y-m-d H:i:s', time()) . " : " . $string . PHP_EOL, FILE_APPEND);
    echo $string . PHP_EOL;
}

function getAMTotalAMApps()
{
    global $AMdb;
    $s = 'SELECT count(*) as c FROM AM_APPLICATION WHERE NAME !="DefaultApplication" AND SUBSCRIBER_ID > 1 AND APPLICATION_TIER != "Defaut" AND APPLICATION_STATUS="APPROVED"';
    $result = mysql_query($s, $AMdb);
    if ($result == 0)
        return 0;
    $num = mysql_numrows($result);

    if ($num == 0)
        return 0;
    if (mysql_result($result, 0, "c") == null)
        return 0;
    else
        return (mysql_result($result, 0, "c"));

}

function getAMAPICount()
{
    global $AMdb;

    $s = 'SELECT COUNT(*) as c FROM AM_API;';
    $result = mysql_query($s, $AMdb);
    if ($result == 0)
        return 0;
    $num = mysql_numrows($result);

    if ($num == 0)
        return 0;
    if (mysql_result($result, 0, "c") == null)
        return 0;
    else
        return (mysql_result($result, 0, "c"));
}

function getAMSubscribers()
{
    global $AMdb;

    $s = 'SELECT COUNT(*) as c FROM AM_SUBSCRIBER ;';
    $result = mysql_query($s, $AMdb);
    $num = mysql_numrows($result);

    if ($num == 0)
        return 0;
    if (mysql_result($result, 0, "c") == null)
        return 0;
    else
        return (mysql_result($result, 0, "c"));
}


function getSubscriptionHistory($from, $to, $status)
{
    global $ADMINdb;
    $s = 'SELECT count(*) as c FROM log_subscription WHERE  datetime >="' . $from . ' 00:00:00" AND datetime <="' . $to . ' 23:59:59"  AND status = "' . $status . '';
    $result = mysql_query($s, $ADMINdb);
    if ($result == 0)
        return 0;
    $num = mysql_numrows($result);

    if ($num == 0)
        return 0;
    if (mysql_result($result, 0, "c") == null)
        return 0;
    else
        return (mysql_result($result, 0, "c"));

}

function getTotalSubs($from, $to, $status)
{
    global $ADMINdb;
    $s = 'SELECT count(*) as c FROM subscription_status WHERE  datetime >="' . $from . ' 00:00:00" AND datetime <="' . $to . ' 23:59:59"  AND status = "' . $status . '';
    $result = mysql_query($s, $ADMINdb);
    if ($result == 0)
        return 0;
    $num = mysql_numrows($result);

    if ($num == 0)
        return 0;
    if (mysql_result($result, 0, "c") == null)
        return 0;
    else
        return (mysql_result($result, 0, "c"));

}