package lk.dialog.ideabiz.logger;

import org.apache.log4j.Logger;

import javax.sql.DataSource;

/**
 * Created by Malinda on 7/10/2015.
 */
public class DatabaseLibrary {

    private static DataSource DatabaseSource;
    private static Logger logger;

    public  DataSource getDatabaseSource() {
        return DatabaseSource;
    }

    public  void setDatabaseSource(DataSource databaseSource) {
        DatabaseSource = databaseSource;
    }

    public DatabaseLibrary() {
        logger = Logger.getLogger(DatabaseLibrary.class);
    }


}
