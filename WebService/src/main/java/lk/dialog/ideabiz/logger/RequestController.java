package lk.dialog.ideabiz.logger;

import com.google.gson.Gson;
import lk.dialog.ideabiz.library.JWT;
import lk.dialog.ideabiz.logger.model.META;
import lk.dialog.ideabiz.logger.model.impl.*;
import lk.dialog.ideabiz.model.JWT.JWTTokenInfo;
import org.apache.commons.logging.Log;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.net.ssl.TrustManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;


@Controller
@RequestMapping("/log")
public class RequestController {

    public static Properties prop = null;

    public static String callback = null;
    public static ArrayList<String> jsonPath = null;
    public static List<String> headerToSkip = null;
    private static Logger log = null;
    public static TrustManager[] trustAllCerts;
    private static int timeout = 5000;
    private Gson gson;

    DirectLogger directLogger = null;
    DatabaseLibrary databaseLibrary;
    Logger logger = Logger.getLogger(RequestController.class);


    @RequestMapping(value = "/logermeta", method = RequestMethod.POST)
    @ResponseBody
    public String saveMeta(@RequestBody String body, HttpServletRequest request, HttpServletResponse response) {
        META meta = gson.fromJson(body, META.class);
        logger.info(gson.toJson(meta));

        String sql ="INSERT INTO `log_metadata` (`ID`, `LOGER_NAME`, `KEY`, `VALUE`,`LAST_UPDATE`) VALUES (NULL, ?, ?, ?,CURRENT_TIMESTAMP()) ON DUPLICATE KEY UPDATE `VALUE`=?,`LAST_UPDATE`=CURRENT_TIMESTAMP() ;";
        try {
            Connection dbConnection = databaseLibrary.getDatabaseSource().getConnection();
            try {

                PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);
                preparedStatement.setString(1, meta.getLOGER_NAME());
                preparedStatement.setString(2, meta.getKEY());
                preparedStatement.setString(3, meta.getVALUE());
                preparedStatement.setString(4, meta.getVALUE());
                preparedStatement.executeUpdate();


            } catch (SQLException e) {
                logger.error("Database Error :" + e.getMessage());
            } catch (Exception e) {
                logger.error("Known Error :" + e.getMessage());
            } finally {
                try {
                    if (dbConnection != null && !dbConnection.isClosed())
                        dbConnection.close();
                } catch (SQLException e) {
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "{}";
    }


    @RequestMapping(value = "/payment", method = RequestMethod.POST)
    @ResponseBody
    public String payment(@RequestBody String body, HttpServletRequest request, HttpServletResponse response) {
        Payment payment = gson.fromJson(body, Payment.class);
        directLogger.addLog(payment);
        return "{}";
    }

    @RequestMapping(value = "/amapicalls", method = RequestMethod.POST)
    @ResponseBody
    public String amapicalls(@RequestBody String body, HttpServletRequest request, HttpServletResponse response) {
        AMAPICalls amapiCalls = gson.fromJson(body, AMAPICalls.class);
        logger.info(gson.toJson(amapiCalls));
        directLogger.addLog(amapiCalls);
        return "{}";
    }


    @RequestMapping(value = "/payment/log", method = RequestMethod.POST)
    @ResponseBody
    public String paymentLog(@RequestBody String body, HttpServletRequest request, HttpServletResponse response) {
        body = body.replace("\r\n", "\n");
        String[] lines = body.split("\n");

        logger.warn("TOTAL : " + lines.length);
        for (int x = 0; x < lines.length; x++) {
            String line = lines[x];
            if(!line.contains("[lk.dialog.ideabiz.logger.model.impl.Payment]"))
            {
                logger.info("Skiping : " + line);
                continue;
            }
            line = line.replace("RECORD:ERROR:","`");
            String datetime = line.split("`")[0];
            line = line.split("`")[1];
            logger.info("Processing : " + line);
            datetime = datetime.split(",")[0];


            PaymentWithDatetime payment = gson.fromJson(line, PaymentWithDatetime.class);
            if(payment.getDatetime() == null)
            payment.setDatetime(datetime);
            directLogger.addLog(payment);
            logger.info("COMPLEATED : " + (x +1) + "/" + lines.length);
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }

       //
        return "{}";
    }

    @RequestMapping(value = "/balanceCheck", method = RequestMethod.POST)
    @ResponseBody
    public String balanceCheck(@RequestBody String body, HttpServletRequest request, HttpServletResponse response) {
        BalanceCheck balanceCheck = gson.fromJson(body, BalanceCheck.class);
        directLogger.addLog(balanceCheck);
        return "{}";
    }

    @RequestMapping(value = "/DigitalRewards", method = RequestMethod.POST)
    @ResponseBody
    public String digitalRewards(@RequestBody String body, HttpServletRequest request, HttpServletResponse response) {
        DigitalRewards digitalRewards = gson.fromJson(body, DigitalRewards.class);
        directLogger.addLog(digitalRewards);
        return "{}";
    }

    @RequestMapping(value = "/location", method = RequestMethod.POST)
    @ResponseBody
    public String location(@RequestBody String body, HttpServletRequest request, HttpServletResponse response) {
        Location location = gson.fromJson(body, Location.class);
        directLogger.addLog(location);
        return "{}";
    }

    @RequestMapping(value = "/smsin", method = RequestMethod.POST)
    @ResponseBody
    public String smsin(@RequestBody String body, HttpServletRequest request, HttpServletResponse response) {
        SMSInbound smsin = gson.fromJson(body, SMSInbound.class);
        directLogger.addLog(smsin);
        return "{}";
    }

    @RequestMapping(value = "/smsout", method = RequestMethod.POST)
    @ResponseBody
    public String smsout(@RequestBody String body, HttpServletRequest request, HttpServletResponse response) {
        SMSOutbound smsout = gson.fromJson(body, SMSOutbound.class);

        List<String> l = new ArrayList<String>();

        String msg = smsout.getRequestClientCorrelator();


        // SMSOutbound n = new SMSOutbound(smsout.getVersion(),smsout.getAppid(),l,"SENDER","SENDER","FINAL","C1","c2","SENT",msg);
//        directLogger.addLog(n);
        return "{}";
    }

    @RequestMapping(value = "/ussd", method = RequestMethod.POST)
    @ResponseBody
    public String ussd(@RequestBody String body, HttpServletRequest request, HttpServletResponse response) {
        USSDRequest ussd = gson.fromJson(body, USSDRequest.class);
        directLogger.addLog(ussd);
        String header = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0=.eyJpc3MiOiJ3c28yLm9yZy9wcm9kdWN0cy9hbSIsImV4cCI6MTQ0MjgzMDI1NDIwNCwiaHR0cDovL3dzbzIub3JnL2NsYWltcy9zdWJzY3JpYmVyIjoiYWRtaW4iLCJodHRwOi8vd3NvMi5vcmcvY2xhaW1zL2FwcGxpY2F0aW9uaWQiOiI0NiIsImh0dHA6Ly93c28yLm9yZy9jbGFpbXMvYXBwbGljYXRpb25uYW1lIjoiQVBJVGVzdCIsImh0dHA6Ly93c28yLm9yZy9jbGFpbXMvYXBwbGljYXRpb250aWVyIjoiVW5saW1pdGVkIiwiaHR0cDovL3dzbzIub3JnL2NsYWltcy9hcGljb250ZXh0IjoiL3Vzc2QiLCJodHRwOi8vd3NvMi5vcmcvY2xhaW1zL3ZlcnNpb24iOiJ2MiIsImh0dHA6Ly93c28yLm9yZy9jbGFpbXMvdGllciI6IjYwdHBtIiwiaHR0cDovL3dzbzIub3JnL2NsYWltcy9rZXl0eXBlIjoiUFJPRFVDVElPTiIsImh0dHA6Ly93c28yLm9yZy9jbGFpbXMvdXNlcnR5cGUiOiJBUFBMSUNBVElPTiIsImh0dHA6Ly93c28yLm9yZy9jbGFpbXMvZW5kdXNlciI6ImFub255bW91cyIsImh0dHA6Ly93c28yLm9yZy9jbGFpbXMvZW5kdXNlclRlbmFudElkIjoiLTEyMzQifQ==.";
        JWTTokenInfo jwt;
        try {
            jwt = JWT.tokenInfo(header);
            Integer a = (JWT.getApplicationIdInt(header));
            a = a;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "{}";
    }

    public RequestController() {
        gson = new Gson();
        // databaseLibrary = new DatabaseLibrary();
          //directLogger = new DirectLogger(databaseLibrary.getDatabaseSource());
  directLogger = new DirectLogger();
        databaseLibrary = new DatabaseLibrary();

    }


}