package lk.dialog.ideabiz.logger.model;

/**
 * Created by Malinda_07654 on 2/3/2017.
 */
public class META {
    String LOGER_NAME;
    String KEY;
    String VALUE;

    public String getLOGER_NAME() {
        return LOGER_NAME;
    }

    public void setLOGER_NAME(String LOGER_NAME) {
        this.LOGER_NAME = LOGER_NAME;
    }

    public String getKEY() {
        return KEY;
    }

    public void setKEY(String KEY) {
        this.KEY = KEY;
    }

    public String getVALUE() {
        return VALUE;
    }

    public void setVALUE(String VALUE) {
        this.VALUE = VALUE;
    }
}
