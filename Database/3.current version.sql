CREATE DATABASE  IF NOT EXISTS `ideabizlog` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ideabizlog`;

-- Table structure for table `digitalrewards_reward`
--

DROP TABLE IF EXISTS `digitalrewards_reward`;

CREATE TABLE `digitalrewards_reward` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `unix_time` int(10) DEFAULT NULL,
  `version` varchar(5) NOT NULL,
  `operator` varchar(10) NOT NULL,
  `appid` int(5) NOT NULL,
  `type` varchar(20) NOT NULL,
  `action` varchar(20) NOT NULL,
  `amount` double NOT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `clientCorrelator` varchar(255) DEFAULT NULL,
  `rewardsTag` varchar(100) DEFAULT NULL,
  `pin` varchar(50) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location_lbs`
--

DROP TABLE IF EXISTS `location_lbs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_lbs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `unix_time` int(10) DEFAULT NULL,
  `version` varchar(5) NOT NULL,
  `operator` varchar(10) NOT NULL,
  `appid` int(5) NOT NULL,
  `address` varchar(20) NOT NULL,
  `requestedAccuracy` int(6) DEFAULT NULL,
  `locationRetrievalStatus` varchar(20) NOT NULL,
  `accuracy` double NOT NULL,
  `status` varchar(20) NOT NULL,
  `LBS_Time` bigint(15) DEFAULT NULL,
  `total_time` bigint(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_balancecheck`
--

DROP TABLE IF EXISTS `payment_balancecheck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_balancecheck` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `unix_time` int(10) DEFAULT NULL,
  `version` varchar(5) NOT NULL,
  `operator` varchar(10) NOT NULL,
  `appid` int(5) NOT NULL,
  `address` varchar(20) NOT NULL,
  `msisdntype` char(1) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `CG_Time` bigint(15) DEFAULT NULL,
  `total_time` bigint(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_transaction`
--

DROP TABLE IF EXISTS `payment_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_transaction` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `unix_time` int(10) DEFAULT NULL,
  `version` varchar(5) NOT NULL,
  `operator` varchar(10) NOT NULL,
  `appid` int(5) NOT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `msisdntype` char(1) DEFAULT NULL,
  `chargingtype` varchar(10) NOT NULL COMMENT 'SPECIAL,DEBIT',
  `reasoncode` varchar(4) NOT NULL,
  `dialog_ref` varchar(50) DEFAULT NULL,
  `application_ref` varchar(50) DEFAULT NULL,
  `cg_ref` varchar(50) DEFAULT NULL,
  `amount` double NOT NULL,
  `tax` double NOT NULL,
  `currency` varchar(3) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `channel` varchar(10) DEFAULT NULL,
  `client_correlator` varchar(200) NOT NULL,
  `client_correlator_hash` char(32) DEFAULT NULL,
  `requested_status` varchar(25) NOT NULL,
  `result_status` varchar(25) NOT NULL,
  `result_description` varchar(25) DEFAULT NULL,
  `CG_Time` bigint(15) DEFAULT NULL,
  `total_time` bigint(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dialog_ref_UNIQUE` (`dialog_ref`),
  KEY `appid` (`appid`,`reasoncode`,`dialog_ref`,`application_ref`,`cg_ref`),
  KEY `unixTime` (`unix_time`),
  KEY `unixTimeApp` (`unix_time`,`appid`),
  KEY `datetime` (`datetime`),
  KEY `reason` (`reasoncode`),
  KEY `reasondate` (`reasoncode`,`datetime`),
  KEY `reasonUnixDate` (`unix_time`,`reasoncode`),
  KEY `msisdn` (`msisdn`),
  KEY `msisdnApp` (`appid`,`msisdn`),
  KEY `msisdnUnix` (`unix_time`,`msisdn`),
  KEY `unixStatus` (`unix_time`,`result_status`),
  KEY `unixAppStatus` (`unix_time`,`appid`,`result_status`),
  KEY `client_correlator_hash` (`client_correlator_hash`),
  KEY `appid_clientcorelator` (`appid`,`client_correlator_hash`),
  KEY `serverRef` (`dialog_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `smsmessaging_inbound`
--

DROP TABLE IF EXISTS `smsmessaging_inbound`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `smsmessaging_inbound` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `unix_time` int(10) DEFAULT NULL,
  `version` varchar(5) NOT NULL,
  `operator` varchar(10) NOT NULL,
  `appid` int(5) NOT NULL,
  `destinationAddress` varchar(20) NOT NULL,
  `messageId` varchar(100) NOT NULL,
  `senderAddress` varchar(20) NOT NULL,
  `msisdntype` char(1) DEFAULT NULL,
  `length` int(4) NOT NULL,
  `type` varchar(20) NOT NULL,
  `messageCount` int(2) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `appStatusCode` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `smsmessaging_outbound`
--

DROP TABLE IF EXISTS `smsmessaging_outbound`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `smsmessaging_outbound` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `unix_time` int(10) DEFAULT NULL,
  `version` varchar(5) NOT NULL,
  `operator` varchar(10) NOT NULL,
  `appid` int(5) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `addressCount` int(5) DEFAULT NULL,
  `senderAddress` varchar(20) NOT NULL,
  `msisdntype` char(1) DEFAULT NULL,
  `senderName` varchar(20) NOT NULL,
  `finalSender` varchar(20) NOT NULL,
  `requestClientCorrelator` varchar(100) NOT NULL,
  `responseClientCorrelator` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `messageCount` int(5) NOT NULL,
  `length` int(5) NOT NULL,
  `messageStatus` varchar(20) DEFAULT NULL,
  `appStatusCode` varchar(20) DEFAULT NULL,
  `messageRef` varchar(255) DEFAULT NULL,
  `batchRef` varchar(255) DEFAULT NULL,
  `deleveredTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `appid` (`appid`),
  KEY `smsref` (`messageRef`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ussd_request`
--

DROP TABLE IF EXISTS `ussd_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ussd_request` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `unix_time` int(10) DEFAULT NULL,
  `version` varchar(5) NOT NULL,
  `operator` varchar(10) NOT NULL,
  `appid` int(5) NOT NULL,
  `address` varchar(20) NOT NULL,
  `shortCode` varchar(20) NOT NULL,
  `keyword` varchar(20) DEFAULT NULL,
  `clientCorrelator` varchar(100) DEFAULT NULL,
  `ussdAction` varchar(20) NOT NULL,
  `sessionId` varchar(50) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `direction` varchar(10) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `length` int(5) DEFAULT NULL,
  `applicationStatus` varchar(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `requestId` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50536 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-25 16:14:55
