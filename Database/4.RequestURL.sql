
CREATE TABLE IF NOT EXISTS `request_url` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `unix_time` int(10) DEFAULT NULL,
  `version` varchar(5) NOT NULL,
  `appid` int(5) NOT NULL,
  `URL` text NOT NULL,
  `URLHash` varchar(32) NOT NULL,
  `API` varchar(20) NOT NULL,
  `APIVersion` varchar(20) NOT NULL,
  `APIMethod` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `errorCode` varchar(20) DEFAULT NULL,
  `statusCode` int(3) DEFAULT NULL,
  `responseTime` int(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
