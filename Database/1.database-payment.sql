-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2015 at 06:30 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Database: `ideabizlog`
--

-- --------------------------------------------------------

--
-- Table structure for table `payment_transaction`
--

CREATE TABLE IF NOT EXISTS `payment_transaction` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `version` varchar(5) NOT NULL,
  `appid` int(5) NOT NULL,
  `chargingtype` varchar(10) NOT NULL COMMENT 'SPECIAL,DEBIT',
  `reasoncode` varchar(4) NOT NULL,
  `dialog_ref` varchar(50) DEFAULT NULL,
  `application_ref` varchar(50) DEFAULT NULL,
  `cg_ref` varchar(50) DEFAULT NULL,
  `amount` double NOT NULL,
  `tax` double NOT NULL,
  `currency` varchar(3) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `channel` varchar(10) DEFAULT NULL,
  `client_correlator` varchar(50) NOT NULL,
  `requested_status` varchar(25) NOT NULL,
  `result_status` varchar(25) NOT NULL,
  `result_description` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `appid` (`appid`,`reasoncode`,`dialog_ref`,`application_ref`,`cg_ref`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

