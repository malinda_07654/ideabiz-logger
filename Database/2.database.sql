-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2015 at 08:01 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `ideabizlog`
--

-- --------------------------------------------------------

--
-- Table structure for table `digitalrewards_reward`
--

CREATE TABLE IF NOT EXISTS `digitalrewards_reward` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `unix_time` int(10) DEFAULT NULL,
  `version` varchar(5) NOT NULL,
  `appid` int(5) NOT NULL,
  `type` varchar(20) NOT NULL,
  `action` varchar(20) NOT NULL,
  `amount` double NOT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `clientCorrelator` varchar(255) DEFAULT NULL,
  `rewardsTag` varchar(100) DEFAULT NULL,
  `pin` varchar(50) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `location_lbs`
--

CREATE TABLE IF NOT EXISTS `location_lbs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `unix_time` int(10) DEFAULT NULL,
  `version` varchar(5) NOT NULL,
  `appid` int(5) NOT NULL,
  `address` varchar(20) NOT NULL,
  `requestedAccuracy` int(6) DEFAULT NULL,
  `locationRetrievalStatus` varchar(20) NOT NULL,
  `accuracy` double NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payment_balancecheck`
--

CREATE TABLE IF NOT EXISTS `payment_balancecheck` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `unix_time` int(10) DEFAULT NULL,
  `version` varchar(5) NOT NULL,
  `appid` int(5) NOT NULL,
  `address` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payment_transaction`
--

CREATE TABLE IF NOT EXISTS `payment_transaction` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `unix_time` int(10) DEFAULT NULL,
  `version` varchar(5) NOT NULL,
  `appid` int(5) NOT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `chargingtype` varchar(10) NOT NULL COMMENT 'SPECIAL,DEBIT',
  `reasoncode` varchar(4) NOT NULL,
  `dialog_ref` varchar(50) DEFAULT NULL,
  `application_ref` varchar(50) DEFAULT NULL,
  `cg_ref` varchar(50) DEFAULT NULL,
  `amount` double NOT NULL,
  `tax` double NOT NULL,
  `currency` varchar(3) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `channel` varchar(10) DEFAULT NULL,
  `client_correlator` varchar(50) NOT NULL,
  `requested_status` varchar(25) NOT NULL,
  `result_status` varchar(25) NOT NULL,
  `result_description` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `appid` (`appid`,`reasoncode`,`dialog_ref`,`application_ref`,`cg_ref`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `smsmessaging_inbound`
--

CREATE TABLE IF NOT EXISTS `smsmessaging_inbound` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `unix_time` int(10) DEFAULT NULL,
  `version` varchar(5) NOT NULL,
  `appid` int(5) NOT NULL,
  `destinationAddress` varchar(20) NOT NULL,
  `messageId` varchar(30) NOT NULL,
  `senderAddress` varchar(20) NOT NULL,
  `length` int(4) NOT NULL,
  `type` varchar(20) NOT NULL,
  `messageCount` int(2) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `appStatusCode` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `smsmessaging_outbound`
--

CREATE TABLE IF NOT EXISTS `smsmessaging_outbound` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `unix_time` int(10) DEFAULT NULL,
  `version` varchar(5) NOT NULL,
  `appid` int(5) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `addressCount` int(5) DEFAULT NULL,
  `senderAddress` varchar(20) NOT NULL,
  `senderName` varchar(20) NOT NULL,
  `finalSender` varchar(20) NOT NULL,
  `requestClientCorrelator` varchar(100) NOT NULL,
  `responseClientCorrelator` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `messageCount` int(5) NOT NULL,
  `length` int(5) NOT NULL,
  `messageStatus` varchar(20) DEFAULT NULL,
  `appStatusCode` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ussd_request`
--

CREATE TABLE IF NOT EXISTS `ussd_request` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `unix_time` int(10) DEFAULT NULL,
  `version` varchar(5) NOT NULL,
  `appid` int(5) NOT NULL,
  `address` varchar(20) NOT NULL,
  `shortCode` varchar(20) NOT NULL,
  `keyword` varchar(20) DEFAULT NULL,
  `clientCorrelator` varchar(100) DEFAULT NULL,
  `ussdAction` varchar(20) NOT NULL,
  `sessionId` varchar(50) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `direction` varchar(10) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `length` int(5) DEFAULT NULL,
  `applicationStatus` varchar(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `requestId` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
